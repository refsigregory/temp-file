<?php include('../_partials/top.php') ?>

<h1 class="page-header">Data Pengguna</h1>
<?php include('_partials/menu.php') ?>

<form action="store.php" method="post">
<h3>A. Data Pribadi</h3>
<table class="table table-striped table-middle">
  <tr>
    <th width="20%">Nama </th>
    <td width="1%">:</td>
    <td><input type="text" class="form-control" name="nama_user" required></td>
  </tr>
  <tr>
    <th>Nama pengguna</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="username_user" required></td>
  </tr>
  <tr>
    <th>Kata Sandi</th>
    <td>:</td>
    <td><input type="password" class="form-control" name="password_user" required></td>
  </tr>
  <tr>
    <th>Keterangan</th>
    <td>:</td>
    <td><textarea class="form-control" name="keterangan_user" ></textarea></td>
  </tr>
  <tr>
    <th>Status</th>
    <td>:</td>
    <td>
      <select class="form-control selectpicker" name="status_user" required>
        <option value="" selected disabled>- pilih -</option>
        <option value="Lurah">Lurah</option>
		 <option value="staf">Staf</option>
        <option value="RT">RT</option>
      </select>
    </td>
  </tr>
</table>

<h3>B. Data Alamat</h3>
<table class="table table-striped table-middle">
  <tr>
    <th width="20%">Desa/Kelurahan</th>
    <td width="1%">:</td>
    <td><input type="text" class="form-control" name="desa_kelurahan_user" onkeypress="return event.charCode < 48 || event.charCode >57" required></td>
  </tr>
  <tr>
		<th>Kecamatan</th>
		<td>:</td>
		<td><input type="text" class="form-control" name="kecamatan_user" onkeypress="return event.charCode < 48 || event.charCode >57"required></td>
	  </tr>
  <tr>
    <th>Kabupaten/Kota</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="kabupaten_kota_user" onkeypress="return event.charCode < 48 || event.charCode >57" required></td>
  </tr>
  <tr>
    <th>Provinsi</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="provinsi_user" onkeypress="return event.charCode < 48 || event.charCode >57" required></td>
  </tr>
  <tr>
    <th>Negara</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="negara_user" onkeypress="return event.charCode < 48 || event.charCode >57"required></td>
  </tr>
  <tr>
    <th>Lingkungan</th>
    <td>:</td>
    <td> <select class="form-control selectpicker" name="lingkungan_user" required>
        <option value="" selected disabled>- pilih -</option>
       <option value="001">1</option>
		 <option value="002">2</option>
		  <option value="003">3</option>
		   <option value="004">4</option>
		    <option value="005">5</option>
			 <option value="006">6</option>
      </select> </td>
  </tr>
  <tr>
    <th>RT</th>
    <td>:</td>
    <td> <select class="form-control selectpicker" name="rt_user" required>
        <option value="" selected disabled>- pilih -</option>
        <option value="001">001</option>
		 <option value="002">002</option>

      </select> </td>
  </tr>
  <tr>
    <th>RW</th>
    <td>:</td>
     <td> <select class="form-control selectpicker" name="rw_user" required>
        <option value="" selected disabled>- pilih -</option>
        <option value="001">001</option>
		 <option value="002">002</option>
		  <option value="003">003</option>	
		 
      </select> </td>
  </tr>
</table>

<button type="submit" class="btn btn-primary btn-lg">
  <i class="glyphicon glyphicon-floppy-save"></i> Simpan
</button>
</form>

<?php include('../_partials/bottom.php') ?>
