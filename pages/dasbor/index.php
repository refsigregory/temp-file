<?php include('../_partials/top.php') ?>

<h1 class="page-header">Beranda</h1>

<?php include('data-index.php') ?>

<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="panel panel-primary">
      <div class="panel-body">
        <h3>Data Warga</h3>
        <p>
          Total ada <?php echo $jumlah_warga['total'] ?> data warga. <?php echo $jumlah_warga_l['total'] ?> di antaranya laki-laki, dan <?php echo $jumlah_warga_p['total'] ?> diantaranya perempuan.
        </p>
        <p>
           penduduk berusia di atas 17 tahun berjumlah <?php echo $jumlah_warga_ld_17['total'] ?> orang, dan di bawah 17 tahun berjumlah <?php echo $jumlah_warga_kd_17['total'] ?> orang.
        </p>
      </div>
      <div class="panel-footer">
        <?php if ($_SESSION['user']['status_user'] != 'RT'): ?>
		<a href="../warga" class="btn btn-primary" role="button">
          <span class="glyphicon glyphicon-book"></span> Detail »
        </a>
		<?php endif; ?>
      </div>
    </div>
  </div>

  <div class="col-sm-6 col-md-4">
    <div class="panel panel-primary">
      <div class="panel-body">
        <h3>Data Kartu Keluarga</h3>
        <p>Total ada <?php echo $jumlah_kartu_keluarga['total'] ?> data kartu keluarga di kelurahan Tatahadeng </p>
		<p>dimana data dalam kartu keluarga diambil dari data warga yang tinggal di kelurahan Tatahadeng </p><p></p>
		

      </div>
      <div class="panel-footer">
		<?php if ($_SESSION['user']['status_user'] != 'RT'): ?>
		<a href="../kartu-keluarga" class="btn btn-primary" role="button">
          <span class="glyphicon glyphicon-inbox"></span> Detail »
        </a>
		<?php endif; ?>
      </div>
    </div>
  </div>

  <div class="col-sm-6 col-md-4">
    <div class="panel panel-primary">
      <div class="panel-body">
        <h3>Data Mutasi</h3>
        <p>
          Total ada <?php echo $jumlah_mutasi['total'] ?> data mutasi. <?php echo $jumlah_mutasi_l['total'] ?> di antaranya laki-laki, dan <?php echo $jumlah_mutasi_p['total'] ?> diantaranya perempuan.
        </p>
		<p>dimana dalam data mutasi berisikan data dari warga yang melakukan mutasi pindah di kelurahan Tatahadeng</p>
      </div>
      <div class="panel-footer">
        <?php if ($_SESSION['user']['status_user'] != 'RT'): ?>
		<a href="../mutasi" class="btn btn-primary" role="button">
          <span class="glyphicon glyphicon-export"></span> Detail »
        </a>
		<?php endif; ?>
      </div>
    </div>
  </div>
</div>
 <div class="col-sm-6 col-md-4">
    <div class="panel panel-primary">
      <div class="panel-body">
        <div id="chartContainer" style="height: 370px; width: 100%;"></div>
      </div>
    </div>
  </div>
  
   <div class="col-sm-6 col-md-4">
    <div class="panel panel-primary">
      <div class="panel-body">
        <div id="chartContainer1" style="height: 370px; width: 100%;"></div>
      </div>
    </div>
  </div>
  
     <div class="col-sm-6 col-md-4">
    <div class="panel panel-primary">
      <div class="panel-body">
        <div id="chartContainer2" style="height: 370px; width: 100%;"></div>
		</div>
		</div>
	</div>
	
	<div class="col-sm-6 col-md-4">
    <div class="panel panel-primary">
      <div class="panel-body">
        <div id="chartContainer3" style="height: 370px; width: 100%;"></div>
		</div>
		</div>
	</div>
	
	<div class="col-sm-6 col-md-4">
    <div class="panel panel-primary">
      <div class="panel-body">
        <div id="chartContainer4" style="height: 370px; width: 100%;"></div>
		</div>
		</div>
	</div>
	
	 <div class="panel-body">
        <div id="chartContainer4" style="height: 370px; width: 100%;"></div>
		</div>

<form method="POST">
	<tr>
	    <th>Periode</th>
    <td>:</td> <td> <select class="form-control selectpicker" name="periode" required >
        <option value="" selected disabled>- pilih -</option>
		<?php
	
	$sql = mysqli_query($db,"SELECT YEAR(created_at) as 'tahun' from warga group by tahun order by tahun ASC");
	if(mysqli_num_rows($sql) != 0){
		while($row = mysqli_fetch_assoc($sql)){
			echo '<option>'.$row['tahun'].'</option>';
		}
	}
	?>
 
      </select> </td>
	  
	  
	  	
 
	  <button type="submit" name="pilih_periode" class="btn btn-primary btn-lg">
  <i class="glyphicon glyphicon-floppy-save"></i> Pilih Periode
</button> <br>
	  </form>
	    <form method="POST">
     <th>Lingkungan</th>
    <td>:</td> <td> <select class="form-control selectpicker" name="lingkungan_user" >
        <option value="" selected disabled>- pilih -</option>
    <?php
	$sel_lingkungan=$_GET['periode'];
	$sql = mysqli_query($db,"SELECT DISTINCT lingkungan_warga as 'ling' from warga where YEAR(created_at)='$sel_lingkungan'");
	if(mysqli_num_rows($sql) != 0){
		while($row = mysqli_fetch_assoc($sql)){
			echo '<option>'.$row['ling'].'</option>';
		}
	}
	?>
      </select> </td>
	  
	<button type="submit" name="pilih_lingkungan" class="btn btn-primary btn-lg">
  <i class="glyphicon glyphicon-floppy-save"></i> Pilih Lingkungan
</button>

  </tr>
  </form>
	
  <?php  
  if(isset($_POST['pilih_periode'])): 
  $lingkungan_pilih=$_POST['lingkungan_user'];
  $periode_pilih=$_POST['periode'];
  echo "<script>location='index.php?periode=$periode_pilih 	' </script>";
  
 
 endif ?>
  <?php  
  if(isset($_POST['pilih_lingkungan'])): 
  $lingkungan_pilih=$_POST['lingkungan_user'];
  $periode_pilih=$_POST['periode'];
  $periode=$_GET['periode'];
  echo "<script>location='index.php?lingkungan=$lingkungan_pilih&periode=$periode ' </script>";
  
 
 endif ?>
  
  
		<div class="col-sm-5 col-md-3">
    <div class="panel panel-primary">
      <div class="panel-body">
        <div id="chartContainer6" style="height: 300px; width: 100%;"></div>
		</div>
		</div>
	</div>
	
	<div class="col-sm-5 col-md-3">
    <div class="panel panel-primary">
      <div class="panel-body">
        <div id="chartContainer7" style="height: 300px; width: 100%;"></div>
		</div>
		</div>
	</div>
	
	<div class="col-sm-5 col-md-3">
    <div class="panel panel-primary">
      <div class="panel-body">
        <div id="chartContainer5" style="height: 300px; width: 100%;"></div>
		</div>
		</div>
	</div>
	
	<div class="col-sm-5 col-md-3">
    <div class="panel panel-primary">
      <div class="panel-body">
        <div id="chartContainer8" style="height: 300px; width: 100%;"></div>
		</div>
		</div>
	</div>
	
	<div class="col-sm-5 col-md-3">
    <div class="panel panel-primary">
      <div class="panel-body">
        <div id="chartContainer9" style="height: 300px; width: 100%;"></div>
		</div>
		</div>
	</div>
	<div class="col-sm-5 col-md-3">
    <div class="panel panel-primary">

     	<div id="chartContainer10" style="height: 370px; width: 100%;"></div>
	
		</div>
	</div>
	
		<div class="col-sm-5 col-md-3">
    <div class="panel panel-primary">

     	<div id="chartContainer11" style="height: 370px; width: 100%;"></div>
	
		</div>
	</div>

	


<?php include('../_partials/bottom.php') ?>
