<?php
session_start();

// jika sudah login, alihkan ke halaman dasbor
if (isset($_SESSION['user'])) {
  header('Location: ../dasbor/index.php');
  exit();
}
?>

<?php include('../_partials/top-login.php') ?>

<div class="row" style="margin-top: 75px">
  <div class="col-md-4 col-md-offset-4">
    <div class="well">

      <form class="form-signin" method="post" action="proses-login.php">
        <h2 class="form-signin-heading text-center">
          <img src="../../assets/img/logositaro.jpeg" width="50" height="50">
		  <br style="margin-bottom: 25px">
		  <p class="text-center"; style="font-weight : bold; font-size : 20px; color :black">APLIKASI PENGELOLAAN DATA PENDUDUK KELURAHAN TATAHADENG</p>          
        </h2>        
        <input type="text" name="username_user" class="form-control" placeholder="Nama Pengguna" autofocus required>

        <input type="password" name="password_user" class="form-control" placeholder="Kata Sandi" required>

        <button class="btn btn-lg btn-primary btn-block" type="submit">
          <i class="glyphicon glyphicon-log-in"></i> Masuk
        </button>
      </form>
    </div>
  </div>
</div>

<?php include('../_partials/bottom-login.php') ?>
