<?php
session_start();
if (!isset($_SESSION['user'])) {
  // jika user belum login
  header('Location: ../login');
  exit();
}
include('../../config/koneksi.php');
?>

<?php
if(isset($_GET['periode']) ){
		
		$periode=$_GET['periode'];
	
	
} else{
		
	$periode=date("Y");
}
if(isset($_GET['lingkungan'])&& isset($_GET['periode']) ){
		$lingkungan=$_GET['lingkungan'];
		$periode=$_GET['periode'];
	
	
} else{
	$lingkungan=1;

}
	$periodefix=date("Y");
	
	$siswa = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periodefix-01-01' and '$periodefix-12-31' AND pekerjaan_warga = 'Siswa'");
	$datasiswa = mysqli_num_rows($siswa);
	
	$mahasiswa = mysqli_query($db, "SELECT * FROM warga WHERE  created_at BETWEEN '$periodefix-01-01' and '$periodefix-12-31' AND pekerjaan_warga = 'Mahasiswa'");
	$datamahasiswa = mysqli_num_rows($mahasiswa);
	
	$guru = mysqli_query($db, "SELECT * FROM warga WHERE  created_at BETWEEN '$periodefix-01-01' and '$periodefix-12-31' and pekerjaan_warga = 'Guru'");
	$dataguru = mysqli_num_rows($guru);
	
	$pns1 = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periodefix-01-01' and '$periodefix-12-31' and pekerjaan_warga = 'PNS'");
	$datapns = mysqli_num_rows($pns1);
	
	$petani = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periodefix-01-01' and '$periodefix-12-31' and pekerjaan_warga = 'Siswa'");
	$datapetani = mysqli_num_rows($petani);
	
	$pelaut = mysqli_query($db, "SELECT * FROM warga WHERE  created_at BETWEEN '$periodefix-01-01' and '$periodefix-12-31' and pekerjaan_warga = 'Pelaut'");
	$datapelaut = mysqli_num_rows($pelaut);
	
	$iburt = mysqli_query($db, "SELECT * FROM warga WHERE  created_at BETWEEN '$periodefix-01-01' and '$periodefix-12-31' and pekerjaan_warga = 'IRT'");
	$datairt = mysqli_num_rows($iburt);

	$wiraswasta = mysqli_query($db, "SELECT * FROM warga WHERE  created_at BETWEEN '$periodefix-01-01' and '$periodefix-12-31' and pekerjaan_warga = 'Wiraswasta'");
	$datawiraswasta = mysqli_num_rows($wiraswasta);
	
 $s=$datasiswa;
 $maha=$datamahasiswa;
 $g=$dataguru;
 $pns=$datapns;
 $pet=$datapetani;
 $pel=$datapelaut;
 $irt=$datairt;
 $wir=$datawiraswasta;
 $totalpekerjaan=($s+$maha+$g+$pns+$pel+$pet+$irt+$wir);
 
$dataPoints = array( 
	array("label"=>"Siswa", "y"=>($s*100/$totalpekerjaan)),
	array("label"=>"Mahasiswa", "y"=>($maha*100/$totalpekerjaan)),
	array("label"=>"Guru", "y"=>($g*100/$totalpekerjaan)),
	array("label"=>"PNS", "y"=>($pns*100/$totalpekerjaan)),
	array("label"=>"Petani", "y"=>($pet*100/$totalpekerjaan)),
	array("label"=>"Pelaut", "y"=>($pel*100/$totalpekerjaan)),
	array("label"=>"IRT", "y"=>($irt*100/$totalpekerjaan)),
	array("label"=>"Wiraswasta", "y"=>($wir*100/$totalpekerjaan))
);


 
$kristen = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periodefix-01-01' and '$periodefix-12-31' and agama_warga = 'Kristen'");
$datakristen = mysqli_num_rows($kristen);
$kr=$datakristen;

$katolik = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periodefix-01-01' and '$periodefix-12-31' and agama_warga = 'Katholik'");
$datakatolik = mysqli_num_rows($katolik);
$kat=$datakatolik;

$islam = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periodefix-01-01' and '$periodefix-12-31' and agama_warga = 'Islam'");
$dataislam = mysqli_num_rows($islam);
$is=$dataislam;

$budha = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periodefix-01-01' and '$periodefix-12-31' and agama_warga = 'Budha'");
$databudha = mysqli_num_rows($budha);
$bud=$databudha;

$hindu = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periodefix-01-01' and '$periodefix-12-31' and agama_warga = 'Hindu'");
$datahindu = mysqli_num_rows($hindu);
$hin=$datahindu;

$konghucu = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periodefix-01-01' and '$periodefix-12-31' and agama_warga = 'Konghucu'");
$datakonghucu = mysqli_num_rows($konghucu);
$kong=$datakonghucu;

$totalagama=($kr+$kat+$is+$bud+$hin+$kong);
 
$dataPoints1 = array( 
	array("label"=>"Kristen", "y"=>($kr*100/$totalagama)),
	array("label"=>"Katholik", "y"=>($kat*100/$totalagama)),
	array("label"=>"Islam", "y"=>($is*100/$totalagama)),
	array("label"=>"Budha", "y"=>($bud*100/$totalagama)),
	array("label"=>"Hindu", "y"=>($hin*100/$totalagama)),
	array("label"=>"Kong Hu Chu", "y"=>($kong*100/$totalagama))
	);
	
	
$wanita = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periodefix-01-01' and '$periodefix-12-31' and jenis_kelamin_warga = 'P'");
$datawanita = mysqli_num_rows($wanita);
$perempuan=$datawanita;

$pria = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periodefix-01-01' and '$periodefix-12-31' and jenis_kelamin_warga = 'L'");
$datapria = mysqli_num_rows($pria);
$laki=$datapria;

 $totaljeniskelamin=($perempuan+$laki);
 
$dataPoints2 = array( 
	array("label"=>"Perempuan", "y"=>($perempuan*100/$totaljeniskelamin)),
	array("label"=>"Laki-laki", "y"=>($laki*100/$totaljeniskelamin))
	);
	
	$remaja = mysqli_query($db, "SELECT 
	COUNT(IF(UMUR BETWEEN 12 and 16,1,NULL)) AS 'remaja' 
	FROM (SELECT *, YEAR(CURDATE())-YEAR(tanggal_lahir_warga) AS UMUR from warga WHERE created_at BETWEEN '$periodefix-01-01' and '$periodefix-12-31') as dummy_table");
	$remaja_row =mysqli_fetch_assoc($remaja);
	$remaja_row["remaja"];

	$balita= mysqli_query($db, "SELECT 
	COUNT(IF(UMUR BETWEEN 0 and 5,1,NULL)) AS 'balita' 
	FROM (SELECT *, YEAR(CURDATE())-YEAR(tanggal_lahir_warga) AS UMUR from warga WHERE created_at BETWEEN '$periodefix-01-01' and '$periodefix-12-31') as dummy_table");
	$balita_row =mysqli_fetch_assoc($balita);
	$balita_row["balita"];
	
	$anak= mysqli_query($db, "SELECT 
	COUNT(IF(UMUR BETWEEN 6 and 11,1,NULL)) AS 'anak' 
	FROM (SELECT *, YEAR(CURDATE())-YEAR(tanggal_lahir_warga) AS UMUR from warga WHERE created_at BETWEEN '$periodefix-01-01' and '$periodefix-12-31') as dummy_table");
	$anak_row =mysqli_fetch_assoc($anak);
	$anak_row["anak"];
	
	$pemuda= mysqli_query($db, "SELECT 
	COUNT(IF(UMUR BETWEEN 17 and 25,1,NULL)) AS 'pemuda' 
	FROM (SELECT *, YEAR(CURDATE())-YEAR(tanggal_lahir_warga) AS UMUR from warga WHERE created_at BETWEEN '$periodefix-01-01' and '$periodefix-12-31') as dummy_table");
	$pemuda_row =mysqli_fetch_assoc($pemuda);
	$pemuda_row["pemuda"];
	
	$dewasa= mysqli_query($db, "SELECT 
	COUNT(IF(UMUR BETWEEN 26 and 59,1,NULL)) AS 'dewasa' 
	FROM (SELECT *, YEAR(CURDATE())-YEAR(tanggal_lahir_warga) AS UMUR from warga WHERE created_at BETWEEN '$periodefix-01-01' and '$periodefix-12-31') as dummy_table");
	$dewasa_row =mysqli_fetch_assoc($dewasa);
	$dewasa_row["dewasa"];
	
	$lansia= mysqli_query($db, "SELECT 
	COUNT(IF(UMUR >=60,1,NULL)) AS 'lansia' 
	FROM (SELECT *, YEAR(CURDATE())-YEAR(tanggal_lahir_warga) AS UMUR from warga WHERE created_at BETWEEN '$periodefix-01-01' and '$periodefix-12-31') as dummy_table");
	$lansia_row =mysqli_fetch_assoc($lansia);
	$lansia_row["lansia"];
	
	$usiabalita=$balita_row["balita"];
	$usiaanak=$anak_row["anak"];
	$usiaremaja=$remaja_row["remaja"];
	$usiapemuda=$pemuda_row["pemuda"];
	$usiadewasa=$dewasa_row["dewasa"];
	$usialansia=$lansia_row["lansia"];
 $totalusia=($usiabalita+$usiaanak+$usiaremaja+$usiapemuda+$usiadewasa+$usialansia);
 
$dataPoints3 = array(
	array("label"=>"Balita", "y"=>($usiabalita*100/$totalusia)),
	array("label"=>"Anak-anak", "y"=>($usiaanak*100/$totalusia)),
	array("label"=>"Remaja", "y"=>($usiaremaja*100/$totalusia)),
	array("label"=>"Pemuda", "y"=>($usiapemuda*100/$totalusia)),
	array("label"=>"Dewasa", "y"=>($usiadewasa*100/$totalusia)),
	array("label"=>"Lansia", "y"=>($usialansia*100/$totalusia))
	);

	$sekolahdasar = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periodefix-01-01' and '$periodefix-12-31' and pendidikan_terakhir_warga = 'SD'");
	$datasd = mysqli_num_rows($sekolahdasar);
	
	$sekolahmenengapertama = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periodefix-01-01' and '$periodefix-12-31' and pendidikan_terakhir_warga = 'SMP'");
	$datasmp = mysqli_num_rows($sekolahmenengapertama);
	
	$sekolahmenengaatas = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periodefix-01-01' and '$periodefix-12-31' and pendidikan_terakhir_warga = 'SMA'");
	$datasma = mysqli_num_rows($sekolahmenengaatas);
	
	$strata1 = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periodefix-01-01' and '$periodefix-12-31' and pendidikan_terakhir_warga = 'S1'");
	$datas1 = mysqli_num_rows($strata1);
	
	$strata2 = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periodefix-01-01' and '$periodefix-12-31' and pendidikan_terakhir_warga = 'S2'");
	$datas2 = mysqli_num_rows($strata2);
	
	$strata3 = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periodefix-01-01' and '$periodefix-12-31' and pendidikan_terakhir_warga = 'S3'");
	$datas3 = mysqli_num_rows($strata3);	
	
	$sd=$datasd;
	$smp=$datasmp;
	$sma=$datasma;
	$s1=$datas1;
	$s2=$datas2;
	$s3=$datas3;
	
 $totalpendidikan=($sd+$smp+$sma+$s1+$s2+$s3);
 
$dataPoints4 = array( 
	array("label"=>"SD", "y"=>($sd*100/$totalpendidikan)),
	array("label"=>"SMP", "y"=>($smp*100/$totalpendidikan)),
	array("label"=>"SMA", "y"=>($sma*100/$totalpendidikan)),
	array("label"=>"S1", "y"=>($s1*100/$totalpendidikan)),
	array("label"=>"S2", "y"=>($s2*100/$totalpendidikan)),
	array("label"=>"S3", "y"=>($s3*100/$totalpendidikan))
	);

	//lingkungan 1
	



	
	$siswa1 = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periode-01-01' and '$periode-12-31' and pekerjaan_warga = 'Siswa' AND lingkungan_warga='$lingkungan'");
	$datasiswa1 = mysqli_num_rows($siswa1);
	
	$mahasiswa1 = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periode-01-01' and '$periode-12-31' and pekerjaan_warga = 'Mahasiswa' AND lingkungan_warga='$lingkungan'");
	$datamahasiswa1 = mysqli_num_rows($mahasiswa1);
	
	$guru1 = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periode-01-01' and '$periode-12-31' and pekerjaan_warga = 'Guru' AND lingkungan_warga='$lingkungan'");
	$dataguru1 = mysqli_num_rows($guru);
	
	$pns11 = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periode-01-01' and '$periode-12-31' and pekerjaan_warga = 'PNS' AND lingkungan_warga='$lingkungan'");
	$datapns1 = mysqli_num_rows($pns11);
	
	$petani1 = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periode-01-01' and '$periode-12-31' and pekerjaan_warga = 'Petani' AND lingkungan_warga='$lingkungan'");
	$datapetani1 = mysqli_num_rows($petani1);
	
	$pelaut1 = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periode-01-01' and '$periode-12-31' and pekerjaan_warga = 'Pelaut' AND lingkungan_warga='$lingkungan'");
	$datapelaut1 = mysqli_num_rows($pelaut1);

	
 $s1=$datasiswa1;
 $maha1=$datamahasiswa1;
 $g1=$dataguru1;
 $pns1=$datapns1;
 $pet1=$datapetani1;
 $pel1=$datapelaut1;
 $totalpekerjaan1=($s1+$maha1+$g1+$pns1+$pel1+$pet1);
 
$dataPoints6 = array( 
	array("label"=>"Siswa", "y"=>($s1*100/$totalpekerjaan1)),
	array("label"=>"Mahasiswa", "y"=>($maha1*100/$totalpekerjaan1)),
	array("label"=>"Guru", "y"=>($g1*100/$totalpekerjaan1)),
	array("label"=>"PNS", "y"=>($pns1*100/$totalpekerjaan1)),
	array("label"=>"Petani", "y"=>($pet1*100/$totalpekerjaan1)),
	array("label"=>"Pelaut", "y"=>($pel1*100/$totalpekerjaan1))
);


$kristen1 = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periode-01-01' and '$periode-12-31' and agama_warga = 'Kristen' AND lingkungan_warga='$lingkungan'");
        $datakristen1 = mysqli_num_rows($kristen1);
        $kr1=$datakristen1;



$katolik1 = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periode-01-01' and '$periode-12-31' and agama_warga = 'Katholik' AND lingkungan_warga='$lingkungan'");
$datakatolik1 = mysqli_num_rows($katolik1);
$kat1=$datakatolik1;

$islam1 = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periode-01-01' and '$periode-12-31' and agama_warga = 'Islam' AND lingkungan_warga='$lingkungan'");
$dataislam1 = mysqli_num_rows($islam1);
$is1=$dataislam1;

$budha1 = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periode-01-01' and '$periode-12-31' and agama_warga = 'Budha' AND lingkungan_warga='$lingkungan'");
$databudha1 = mysqli_num_rows($budha1);
$bud1=$databudha1;

$hindu1 = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periode-01-01' and '$periode-12-31' and agama_warga = 'Hindu' AND lingkungan_warga='$lingkungan'");
$datahindu1 = mysqli_num_rows($hindu1);
$hin1=$datahindu1;

$konghucu1 = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periode-01-01' and '$periode-12-31' and agama_warga = 'Konghucu' AND lingkungan_warga='$lingkungan'");
$datakonghucu1 = mysqli_num_rows($konghucu1);
$kong1=$datakonghucu1;



$totalagama7=($kr1+$kat1+$is1+$bud1+$hin1+$kong1);
 
$dataPoints7 = array( 
	array("label"=>"Kristen", "y"=>($kr1*100/$totalagama7)),
	array("label"=>"Katolik", "y"=>($kat1*100/$totalagama7)),
	array("label"=>"Islam", "y"=>($is1*100/$totalagama7)),
	array("label"=>"Budha", "y"=>($bud1*100/$totalagama7)),
	array("label"=>"Hindu", "y"=>($hin1*100/$totalagama7)),
	array("label"=>"Kong Hu Chu", "y"=>($kong1*100/$totalagama7))
	);



$wanita1 = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periode-01-01' and '$periode-12-31' and jenis_kelamin_warga = 'L' AND lingkungan_warga='$lingkungan'");
$datawanita1 = mysqli_num_rows($wanita1);
$perempuan1=$datawanita1;

$pria1 = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periode-01-01' and '$periode-12-31' and jenis_kelamin_warga = 'P' AND lingkungan_warga='$lingkungan'");
$datapria1 = mysqli_num_rows($pria1);
$laki1=$datapria1;

 $totaljeniskelamin1=($perempuan1+$laki1);


$dataPoints5 = array( 
	array("label"=>"Perempuan", "y"=>($perempuan1*100/$totaljeniskelamin1)),
	array("label"=>"Laki-laki", "y"=>($laki1*100/$totaljeniskelamin1))
	);
	
	$remaja1 = mysqli_query($db, "SELECT 
	COUNT(IF(UMUR BETWEEN 12 and 16,1,NULL)) AS 'remaja' 
	FROM (SELECT *, YEAR(CURDATE())-YEAR(tanggal_lahir_warga) AS UMUR from warga WHERE created_at BETWEEN '$periode-01-01' and '$periode-12-31' AND lingkungan_warga='$lingkungan') as dummy_table");
	$remaja_row1=mysqli_fetch_assoc($remaja1);
	$remaja_row1["remaja"];

	$balita1= mysqli_query($db, "SELECT 
	COUNT(IF(UMUR BETWEEN 0 and 5,1,NULL)) AS 'balita' 
	FROM (SELECT *, YEAR(CURDATE())-YEAR(tanggal_lahir_warga) AS UMUR from warga WHERE created_at BETWEEN '$periode-01-01' and '$periode-12-31' AND lingkungan_warga='$lingkungan') as dummy_table");
	$balita_row1 =mysqli_fetch_assoc($balita1);
	$balita_row1["balita"];
	
	$anak1= mysqli_query($db, "SELECT 
	COUNT(IF(UMUR BETWEEN 6 and 11,1,NULL)) AS 'anak' 
	FROM (SELECT *, YEAR(CURDATE())-YEAR(tanggal_lahir_warga) AS UMUR from warga WHERE created_at BETWEEN '$periode-01-01' and '$periode-12-31' AND lingkungan_warga='$lingkungan') as dummy_table");
	$anak_row1 =mysqli_fetch_assoc($anak1);
	$anak_row1["anak"];
	
	$pemuda1= mysqli_query($db, "SELECT 
	COUNT(IF(UMUR BETWEEN 17 and 25,1,NULL)) AS 'pemuda' 
	FROM (SELECT *, YEAR(CURDATE())-YEAR(tanggal_lahir_warga) AS UMUR from warga WHERE created_at BETWEEN '$periode-01-01' and '$periode-12-31' AND lingkungan_warga='$lingkungan') as dummy_table");
	$pemuda_row1 =mysqli_fetch_assoc($pemuda1);
	$pemuda_row1["pemuda"];
	
	$dewasa1= mysqli_query($db, "SELECT 
	COUNT(IF(UMUR BETWEEN 26 and 59,1,NULL)) AS 'dewasa' 
	FROM (SELECT *, YEAR(CURDATE())-YEAR(tanggal_lahir_warga) AS UMUR from warga WHERE created_at BETWEEN '$periode-01-01' and '$periode-12-31' AND lingkungan_warga='$lingkungan') as dummy_table");
	$dewasa_row1 =mysqli_fetch_assoc($dewasa1);
	$dewasa_row1["dewasa"];
	
	$lansia1= mysqli_query($db, "SELECT 
	COUNT(IF(UMUR >=60,1,NULL)) AS 'lansia' 
	FROM (SELECT *, YEAR(CURDATE())-YEAR(tanggal_lahir_warga) AS UMUR from warga WHERE created_at BETWEEN '$periode-01-01' and '$periode-12-31' AND lingkungan_warga='$lingkungan') as dummy_table");
	$lansia_row1 =mysqli_fetch_assoc($lansia1);
	$lansia_row1["lansia"];
	
	$usiabalita1=$balita_row1["balita"];
	$usiaanak1=$anak_row1["anak"];
	$usiaremaja1=$remaja_row1["remaja"];
	$usiapemuda1=$pemuda_row1["pemuda"];
	$usiadewasa1=$dewasa_row1["dewasa"];
	$usialansia1=$lansia_row1["lansia"];
 $totalusia1=($usiabalita1+$usiaanak1+$usiaremaja1+$usiapemuda1+$usiadewasa1+$usialansia1);
 
$dataPoints8 = array(
	array("label"=>"Balita", "y"=>($usiabalita1*100/$totalusia1)),
	array("label"=>"Anak-anak", "y"=>($usiaanak1*100/$totalusia1)),
	array("label"=>"Remaja", "y"=>($usiaremaja1*100/$totalusia1)),
	array("label"=>"Pemuda", "y"=>($usiapemuda1*100/$totalusia1)),
	array("label"=>"Dewasa", "y"=>($usiadewasa1*100/$totalusia1)),
	array("label"=>"Lansia", "y"=>($usialansia1*100/$totalusia1))
	);
	
	$sekolahdasar1 = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periode-01-01' and '$periode-12-31' and pendidikan_terakhir_warga = 'SD' AND lingkungan_warga='$lingkungan' " );
	$datasd1 = mysqli_num_rows($sekolahdasar1);
	
	$sekolahmenengapertama1 = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periode-01-01' and '$periode-12-31' and pendidikan_terakhir_warga = 'SMP' AND lingkungan_warga='$lingkungan'");
	$datasmp1 = mysqli_num_rows($sekolahmenengapertama1);
	
	$sekolahmenengaatas1 = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periode-01-01' and '$periode-12-31' and pendidikan_terakhir_warga = 'SMA' AND lingkungan_warga='$lingkungan'");
	$datasma1 = mysqli_num_rows($sekolahmenengaatas1);
	
	$strata11 = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periode-01-01' and '$periode-12-31' and pendidikan_terakhir_warga = 'S1' AND lingkungan_warga='$lingkungan'");
	$datas11 = mysqli_num_rows($strata11);
	
	$strata21 = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periode-01-01' and '$periode-12-31' and pendidikan_terakhir_warga = 'S2' AND lingkungan_warga='$lingkungan'");
	$datas21 = mysqli_num_rows($strata21);
	
	$strata31 = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periode-01-01' and '$periode-12-31' and pendidikan_terakhir_warga = 'S3	' AND lingkungan_warga='$lingkungan'");
	$datas31 = mysqli_num_rows($strata31);	
	
	$sd1=$datasd1;
	$smp1=$datasmp1;
	$sma1=$datasma1;
	$s11=$datas11;
	$s21=$datas21;
	$s31=$datas31;
	
 $totalpendidikan1=($sd1+$smp1+$sma1+$s11+$s21+$s31);
 
$dataPoints9 = array( 
	array("label"=>"SD", "y"=>($sd1*100/$totalpendidikan1)),
	array("label"=>"SMP", "y"=>($smp1*100/$totalpendidikan1)),
	array("label"=>"SMA", "y"=>($sma1*100/$totalpendidikan1)),
	array("label"=>"S1", "y"=>($s11*100/$totalpendidikan1)),
	array("label"=>"S2", "y"=>($s21*100/$totalpendidikan1)),
	array("label"=>"S3", "y"=>($s31*100/$totalpendidikan1))
	);
	
	// kematian
	$kematianjan = mysqli_query($db, "SELECT * FROM mutasi WHERE created_at BETWEEN '$periode-01-01' and '$periode-01-31' and keterangan_mutasi='meninggal' " );
	$datakematianjan = mysqli_num_rows($kematianjan);
		$kematianfeb = mysqli_query($db, "SELECT * FROM mutasi WHERE created_at BETWEEN '$periode-02-01' and '$periode-02-31' and keterangan_mutasi='meninggal' " );
		$datakematianfeb = mysqli_num_rows($kematianfeb);
	$kematianmar = mysqli_query($db, "SELECT * FROM mutasi WHERE created_at BETWEEN '$periode-03-01' and '$periode-03-31' and keterangan_mutasi='meninggal' " );
	$datakematianmar = mysqli_num_rows($kematianmar);
		$kematianap = mysqli_query($db, "SELECT * FROM mutasi WHERE created_at BETWEEN '$periode-04-01' and '$periode-04-31' and keterangan_mutasi='meninggal' " );
		$datakematianap = mysqli_num_rows($kematianap);
	$kematianmei = mysqli_query($db, "SELECT * FROM mutasi WHERE created_at BETWEEN '$periode-05-01' and '$periode-05-31' and keterangan_mutasi='meninggal' " );
	$datakematianmei = mysqli_num_rows($kematianmei);
		$kematianjun = mysqli_query($db, "SELECT * FROM mutasi WHERE created_at BETWEEN '$periode-06-01' and '$periode-06-31' and keterangan_mutasi='meninggal' " );
		$datakematianjun = mysqli_num_rows($kematianjun);
	$kematianjul = mysqli_query($db, "SELECT * FROM mutasi WHERE created_at BETWEEN '$periode-07-01' and '$periode-07-31' and keterangan_mutasi='meninggal' " );
	$datakematianjul = mysqli_num_rows($kematianjul);
		$kematianagu = mysqli_query($db, "SELECT * FROM mutasi WHERE created_at BETWEEN '$periode-08-01' and '$periode-08-31' and keterangan_mutasi='meninggal' " );
		$datakematianagu = mysqli_num_rows($kematianagu);
	$kematiansep = mysqli_query($db, "SELECT * FROM mutasi WHERE created_at BETWEEN '$periode-09-01' and '$periode-09-31' and keterangan_mutasi='meninggal' " );
	$datakematiansep = mysqli_num_rows($kematiansep);
		$kematianokt = mysqli_query($db, "SELECT * FROM mutasi WHERE created_at BETWEEN '$periode-10-01' and '$periode-10-31' and keterangan_mutasi='meninggal' " );
		$datakematianokt = mysqli_num_rows($kematianokt);
	$kematiannov = mysqli_query($db, "SELECT * FROM mutasi WHERE created_at BETWEEN '$periode-11-01' and '$periode-11-31' and keterangan_mutasi='meninggal' " );
	$datakematiannov = mysqli_num_rows($kematiannov);
		$kematiandes = mysqli_query($db, "SELECT * FROM mutasi WHERE created_at BETWEEN '$periode-12-01' and '$periode-12-31' and keterangan_mutasi='meninggal' " );
		$datakematiandes = mysqli_num_rows($kematiandes);
		
	
	//kelahiran
	$kelahiranjan = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periode-01-01' and '$periode-01-31' and status_kelahiran='1' " );
	$datakelahiranjan = mysqli_num_rows($kelahiranjan);
		$kelahiranfeb = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periode-02-01' and '$periode-02-31' and status_kelahiran='1' " );
		$datakelahiranfeb = mysqli_num_rows($kelahiranfeb);
	$kelahiranmar = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periode-03-01' and '$periode-03-31' and status_kelahiran='1' " );
	$datakelahiranmar = mysqli_num_rows($kelahiranmar);
		$kelahiranap = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periode-04-01' and '$periode-04-31' and status_kelahiran='1' " );
		$datakelahiranap = mysqli_num_rows($kelahiranap);
	$kelahiranmei = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periode-05-01' and '$periode-05-31' and status_kelahiran='1' " );
	$datakelahiranmei = mysqli_num_rows($kelahiranmei);
		$kelahiranjun = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periode-06-01' and '$periode-06-31' and status_kelahiran='1' " );
		$datakelahiranjun = mysqli_num_rows($kelahiranjun);
	$kelahiranjul = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periode-07-01' and '$periode-07-31' and status_kelahiran='1' " );
	$datakelahiranjul = mysqli_num_rows($kelahiranjul);
		$kelahiranagu = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periode-08-01' and '$periode-08-31' and status_kelahiran='1' " );
		$datakelahiranagu = mysqli_num_rows($kelahiranagu);
	$kelahiransep = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periode-09-01' and '$periode-09-31' and status_kelahiran='1' " );
	$datakelahiransep = mysqli_num_rows($kelahiransep);
		$kelahiranokt = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periode-10-01' and '$periode-10-31' and status_kelahiran='1' " );
		$datakelahiranokt = mysqli_num_rows($kelahiranokt);
	$kelahirannov = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periode-11-01' and '$periode-11-31' and status_kelahiran='1' " );
	$datakelahirannov = mysqli_num_rows($kelahirannov);
		$kelahirandes = mysqli_query($db, "SELECT * FROM warga WHERE created_at BETWEEN '$periode-12-01' and '$periode-12-31' and status_kelahiran='1' " );
		$datakelahirandes = mysqli_num_rows($kelahirandes);
	?>
	
	
	
<!DOCTYPE html>
<html lang="en">
  <head>
  <script>
window.onload = function() {
 
 
var chart = new CanvasJS.Chart("chartContainer", {
	theme: "light2",
	animationEnabled: true,
	title: {
		text: "Perkerjaan Warga"+" Periode "+"<?php echo $periodefix ?>"
	},
	data: [{
		type: "pie",
		indexLabel: "{y}",
		yValueFormatString: "#,##0.00\"%\"",
		indexLabelPlacement: "inside",
		indexLabelFontColor: "#36454F",
		indexLabelFontSize: 18,
		indexLabelFontWeight: "bolder",
		showInLegend: true,
		legendText: "{label}",
		dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
	}]
});
chart.render();
 
 
	var chart1 = new CanvasJS.Chart("chartContainer1", {
	theme: "light2",
	animationEnabled: true,
	title: {
		text: "Agama Warga"+" Periode "+"<?php echo $periodefix ?>"
	},
	data: [{
		type: "pie",
		indexLabel: "{y}",
		yValueFormatString: "#,##0.00\"%\"",
		indexLabelPlacement: "inside",
		indexLabelFontColor: "#36454F",
		indexLabelFontSize: 18,
		indexLabelFontWeight: "bolder",
		showInLegend: true,
		legendText: "{label}",
		dataPoints: <?php echo json_encode($dataPoints1, JSON_NUMERIC_CHECK); ?>
	}]
	});
chart1.render();
	
	
		var chart2 = new CanvasJS.Chart("chartContainer2", {
		theme: "light2",
		animationEnabled: true,
		title: {
		text: "Jenis Kelamin Warga"+" Periode "+"<?php echo $periodefix ?>"
		},
		data: [{
		type: "pie",
		indexLabel: "{y}",
		yValueFormatString: "#,##0.00\"%\"",
		indexLabelPlacement: "inside",
		indexLabelFontColor: "#36454F",
		indexLabelFontSize: 18,
		indexLabelFontWeight: "bolder",
		showInLegend: true,
		legendText: "{label}",
		dataPoints: <?php echo json_encode($dataPoints2, JSON_NUMERIC_CHECK); ?>
		}]
		});
		chart2.render();
	
			var chart3 = new CanvasJS.Chart("chartContainer3", {
			theme: "light2",
			animationEnabled: true,
			title: {
			text: "Usia Warga"+" Periode "+"<?php echo $periodefix ?>"
			},
			data: [{
			type: "pie",
			indexLabel: "{y}",
			yValueFormatString: "#,##0.00\"%\"",
			indexLabelPlacement: "inside",
			indexLabelFontColor: "#36454F",
			indexLabelFontSize: 18,
			indexLabelFontWeight: "bolder",
			showInLegend: true,
			legendText: "{label}",
			dataPoints: <?php echo json_encode($dataPoints3, JSON_NUMERIC_CHECK); ?>
			}]
			});
			chart3.render();
	
				var chart4 = new CanvasJS.Chart("chartContainer4", {
				theme: "light2",
				animationEnabled: true,
				title: {
				text: "Pendidikan Warga"+" Periode "+"<?php echo $periodefix ?>"
				},
				data: [{
				type: "pie",
				indexLabel: "{y}",
				yValueFormatString: "#,##0.00\"%\"",
				indexLabelPlacement: "inside",
				indexLabelFontColor: "#36454F",
				indexLabelFontSize: 18,
				indexLabelFontWeight: "bolder",
				showInLegend: true,
				legendText: "{label}",
				dataPoints: <?php echo json_encode($dataPoints4, JSON_NUMERIC_CHECK); ?>
				}]
				});
				chart4.render();
				
		var chart5 = new CanvasJS.Chart("chartContainer5", {
		theme: "light2",
		animationEnabled: true,
		title: {
		text: "Jenis Kelamin Warga di Lingkungan "+"<?php echo $lingkungan ?>"+" Periode "+"<?php echo $periode ?>"
		},
		data: [{
		type: "pie",
		indexLabel: "{y}",
		yValueFormatString: "#,##0.00\"%\"",
		indexLabelPlacement: "inside",
		indexLabelFontColor: "#36454F",
		indexLabelFontSize: 18,
		indexLabelFontWeight: "bolder",
		showInLegend: true,
		legendText: "{label}",
		dataPoints: <?php echo json_encode($dataPoints5, JSON_NUMERIC_CHECK); ?>
		}]
		});
		chart5.render();
			
			var chart6 = new CanvasJS.Chart("chartContainer6", {
			theme: "light2",
			animationEnabled: true,
			title: {
			text: "Pekerjaan Warga di Lingkungan "+"<?php echo $lingkungan?>"+" Periode "+"<?php echo $periode ?>"

			},
			data: [{
			type: "pie",
			indexLabel: "{y}",
			yValueFormatString: "#,##0.00\"%\"",
			indexLabelPlacement: "inside",
			indexLabelFontColor: "#36454F",
			indexLabelFontSize: 18,
			indexLabelFontWeight: "bolder",
			showInLegend: true,
			legendText: "{label}",
			dataPoints: <?php echo json_encode($dataPoints6, JSON_NUMERIC_CHECK); ?>
			}]
			});
			chart6.render();
			
			
				var chart7 = new CanvasJS.Chart("chartContainer7", {
				theme: "light2",
				animationEnabled: true,
				title: {
				text: "Agama Warga di Lingkungan "+"<?php echo $lingkungan ?>"+" Periode "+"<?php echo $periode ?>"
				
				},
				data: [{
				type: "pie",
				indexLabel: "{y}",
				yValueFormatString: "#,##0.00\"%\"",
				indexLabelPlacement: "inside",
				indexLabelFontColor: "#36454F",
				indexLabelFontSize: 18,
				indexLabelFontWeight: "bolder",
				showInLegend: true,
				legendText: "{label}",
				dataPoints: <?php echo json_encode($dataPoints7, JSON_NUMERIC_CHECK); ?>
				}]
				});
				chart7.render();

				var chart8 = new CanvasJS.Chart("chartContainer8", {
				theme: "light2",
				animationEnabled: true,
				title: {
				text: "Usia Warga di Lingkungan "+"<?php echo $lingkungan ?>"+" Periode "+"<?php echo $periode ?>"
				},
				data: [{
				type: "pie",
				indexLabel: "{y}",
				yValueFormatString: "#,##0.00\"%\"",
				indexLabelPlacement: "inside",
				indexLabelFontColor: "#36454F",
				indexLabelFontSize: 18,
				indexLabelFontWeight: "bolder",
				showInLegend: true,
				legendText: "{label}",
				dataPoints: <?php echo json_encode($dataPoints8, JSON_NUMERIC_CHECK); ?>
				}]
				});
				chart8.render();
				
				
				
				var chart9 = new CanvasJS.Chart("chartContainer9", {
				theme: "light2",
				animationEnabled: true,
				title: {
				text: "Pendidikan Warga di Lingkungan "+"<?php echo $lingkungan ?>"+" Periode "+"<?php echo $periode ?>"
				},
				data: [{
				type: "pie",
				indexLabel: "{y}",
				yValueFormatString: "#,##0.00\"%\"",
				indexLabelPlacement: "inside",
				indexLabelFontColor: "#36454F",
				indexLabelFontSize: 18,
				indexLabelFontWeight: "bolder",
				showInLegend: true,
				legendText: "{label}",
				dataPoints: <?php echo json_encode($dataPoints9, JSON_NUMERIC_CHECK); ?>
				}]
				});
				chart9.render();
				
				var kematianjan=<?php echo $datakematianjan ?>;
				var kematianfeb=<?php echo $datakematianfeb ?>;
				var kematianmar=<?php echo $datakematianmar ?>;
				var kematianap=<?php echo $datakematianap ?>;
				var kematianmei=<?php echo $datakematianmei ?>;
				var kematianjun=<?php echo $datakematianjun ?>;
				var kematianjul=<?php echo $datakematianjul ?>;
				var kematianagu=<?php echo $datakematianagu ?>;
				var kematiansep=<?php echo $datakematiansep ?>;
				var kematianokt=<?php echo $datakematianokt ?>;
				var kematiannov=<?php echo $datakematiannov ?>;
				var kematiandes=<?php echo $datakematiandes ?>;
				var chart10 = new CanvasJS.Chart("chartContainer10", {
				animationEnabled: true,
				theme: "light2",
				title:{
				text: "Grafik Kematian "+" Periode "+"<?php echo $periode ?>"
				},
				axisY: {
				title: "Jumlah Kematian (orang)"
				},
				dataPointWidth: 20,
				data: [{
				type: "column",
				yValueFormatString: "#,##0.## orang",
				
				dataPoints: [
       {  y: kematianjan, name: "Januari", label: "Januari" },
       {  y: kematianfeb, name: "Februari", label: "Februari" },
       {  y: kematianmar, name: "Maret", label: "Maret" },
       {  y: kematianap, name: "April", label: "April" },
       {  y: kematianmei, name: "Mei", label: "Mei" },
       {  y: kematianjun, name: "Juni", label: "Juni" },
       { y: kematianjul, name: "Juli", label: "Juli"},
	   {  y: kematianagu, name: "Agustus", label: "Agustus" },
	   {  y: kematiansep, name: "September", label: "September" },
	   {  y: kematianokt, name: "Oktober", label: "Oktober" },
	   {  y: kematiannov, name: "November", label: "November" },
	   {  y: kematiandes, name: "Desember", label: "Desember" }
       ]
     
				}]
				});
				chart10.render();
				

				
				var kelahiranjan=<?php echo $datakelahiranjan ?>;
				var kelahiranfeb=<?php echo $datakelahiranfeb ?>;
				var kelahiranmar=<?php echo $datakelahiranmar ?>;
				var kelahiranap=<?php echo $datakelahiranap ?>;
				var kelahiranmei=<?php echo $datakelahiranmei ?>;
				var kelahiranjun=<?php echo $datakelahiranjun ?>;
				var kelahiranjul=<?php echo $datakelahiranjul ?>;
				var kelahiranagu=<?php echo $datakelahiranagu ?>;
				var kelahiransep=<?php echo $datakelahiransep ?>;
				var kelahiranokt=<?php echo $datakelahiranokt ?>;
				var kelahirannov=<?php echo $datakelahirannov ?>;
				var kelahirandes=<?php echo $datakelahirandes ?>;
				var chart11 = new CanvasJS.Chart("chartContainer11", {
				animationEnabled: true,
				theme: "light2",
				title:{
				text: "Grafik Kelahiran" +" Periode "+"<?php echo $periode ?>"
				},
				axisY: {
				title: "Jumlah Kelahiran (orang)"
				},
				dataPointWidth: 20,
				data: [{
				type: "column",
				yValueFormatString: "#,##0.## orang",
				
				dataPoints: [
       {  y: kelahiranjan, name: "Januari", label: "Januari" },
       {  y: kelahiranfeb, name: "Februari", label: "Februari" },
       {  y: kelahiranmar, name: "Maret", label: "Maret" },
       {  y: kelahiranap, name: "April", label: "April" },
       {  y: kelahiranmei, name: "Mei", label: "Mei" },
       {  y: kelahiranjun, name: "Juni", label: "Juni" },
       { y: kelahiranjul, name: "Juli", label: "Juli"},
	   {  y: kelahiranagu, name: "Agustus", label: "Agustus" },
	   {  y: kelahiransep, name: "September", label: "September" },
	   {  y: kelahiranokt, name: "Oktober", label: "Oktober" },
	   {  y: kelahirannov, name: "November", label: "November" },
	   {  y: kelahirandes, name: "Desember", label: "Desember" }
       ]
     
				}]
				});
				chart11.render();
				

				
				
	}
</script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Aplikasi Pengelolaan Data Kelurahan Tatahadeng">
    <meta name="author" content="Megawati Salindeho">
    <link rel="icon" href="../../../favicon.ico">

    <title>Aplikasi Pengelolaan Data Kelurahan Tatahadeng</title>

    <!-- Bootstrap core CSS -->
    <link href="../../assets/css/bootstrap.min.css" rel="stylesheet">


    <!-- DataTable CSS -->
    <link href="../../assets/css/dataTables.bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../../assets/css/dashboard.css" rel="stylesheet">

    <!-- Date Range Picker style -->
    <link href="../../assets/css/daterangepicker.css" rel="stylesheet">

    <!-- Lightbox style -->
    <link href="../../assets/lib/lightbox/css/lightbox.min.css" rel="stylesheet">

    <!-- Bootstrap Select style-->
    <link rel="stylesheet" href="../../assets/css/bootstrap-select.min.css">
  </head>

  <body>
<script src="../../assets/js/canvasjs.min.js"></script>
    <?php include('../_partials/navbar.php') ?>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <?php include('../_partials/sidebar.php') ?>
        </div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">