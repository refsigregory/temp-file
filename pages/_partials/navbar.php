<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
	  <div class="col-xl-6">
	  <a style="font-size:30px;color:#ffffff" class="navbar-brand" href="../dasbor">Aplikasi Pengelolaan Data Penduduk Kelurahan Tatahadeng </a>
	  </div>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav navbar-right">
        <p style="color:#ffffff" class="navbar-text">Selamat Datang, <?php echo $_SESSION['user']['nama_user'] ?></p>
  
        <li><a style="color:#ffffff" href="../login/logout.php"><i class="glyphicon glyphicon-log-out"></i> Keluar</a></li>
      </ul>
    </div>
  </div>
</nav>
