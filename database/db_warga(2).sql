-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 21, 2019 at 06:57 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_warga`
--

-- --------------------------------------------------------

--
-- Table structure for table `kartu_keluarga`
--

CREATE TABLE `kartu_keluarga` (
  `id_keluarga` int(11) NOT NULL,
  `nomor_keluarga` varchar(16) NOT NULL,
  `id_kepala_keluarga` int(11) NOT NULL,
  `alamat_keluarga` text NOT NULL,
  `desa_kelurahan_keluarga` varchar(30) NOT NULL,
  `kecamatan_keluarga` varchar(30) NOT NULL,
  `kabupaten_kota_keluarga` varchar(30) NOT NULL,
  `provinsi_keluarga` varchar(30) NOT NULL,
  `negara_keluarga` varchar(30) NOT NULL,
  `rt_keluarga` varchar(3) NOT NULL,
  `rw_keluarga` varchar(3) NOT NULL,
  `id_user` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lingkungan_keluarga` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kartu_keluarga`
--

INSERT INTO `kartu_keluarga` (`id_keluarga`, `nomor_keluarga`, `id_kepala_keluarga`, `alamat_keluarga`, `desa_kelurahan_keluarga`, `kecamatan_keluarga`, `kabupaten_kota_keluarga`, `provinsi_keluarga`, `negara_keluarga`, `rt_keluarga`, `rw_keluarga`, `id_user`, `created_at`, `updated_at`, `lingkungan_keluarga`) VALUES
(2, '7109010103180003', 6, 'Dalanisang', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '003', 23, '2019-11-12 04:41:05', '0000-00-00 00:00:00', 1),
(3, '7109010102160003', 9, 'Dalanisang', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 23, '2019-11-12 04:56:07', '0000-00-00 00:00:00', 1),
(4, '7109010110120002', 11, 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 23, '2019-11-12 05:02:24', '0000-00-00 00:00:00', 1),
(5, '7109010206170004', 13, 'Pelabuhan', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 23, '2019-11-12 05:20:23', '0000-00-00 00:00:00', 1),
(6, '7109010404180004', 17, 'Pelabuhan', 'Pelabuhan', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 24, '2019-11-12 05:41:36', '0000-00-00 00:00:00', 1),
(7, '7109010404180002', 19, 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 24, '2019-11-12 06:50:59', '0000-00-00 00:00:00', 1),
(8, '7109010503180005', 22, 'Lewae', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 24, '2019-11-12 07:26:40', '0000-00-00 00:00:00', 1),
(9, '7109010603083837', 24, 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 24, '2019-11-12 07:41:08', '0000-00-00 00:00:00', 1),
(11, '7109010603083841', 31, 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 24, '2019-11-12 08:22:38', '0000-00-00 00:00:00', 1),
(13, '7109010603083843', 34, 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 24, '2019-11-12 08:32:38', '0000-00-00 00:00:00', 1),
(14, '7109010603083850', 38, 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 24, '2019-11-12 08:38:55', '0000-00-00 00:00:00', 1),
(15, '7109010603083845', 40, 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 24, '2019-11-12 08:47:57', '0000-00-00 00:00:00', 1),
(16, '7109010603083850', 44, 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 24, '2019-11-12 08:59:12', '0000-00-00 00:00:00', 1),
(17, '7109010603083851', 47, 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 24, '2019-11-12 09:07:36', '0000-00-00 00:00:00', 1),
(18, '7109010603083853', 50, 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 24, '2019-11-12 09:12:28', '0000-00-00 00:00:00', 1),
(19, '7109010603083856', 52, 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 24, '2019-11-12 10:41:06', '0000-00-00 00:00:00', 1),
(21, '7109010603083917', 68, 'Lansi', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 25, '2019-11-12 12:57:39', '0000-00-00 00:00:00', 2),
(22, '7109010603083930', 74, 'Tatahadeng', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 25, '2019-11-12 13:12:31', '0000-00-00 00:00:00', 2),
(23, '7109010603084007', 76, 'Poso Kedio', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 25, '2019-11-12 13:22:30', '0000-00-00 00:00:00', 2),
(24, '7109010603084193', 80, 'Poso', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 25, '2019-11-12 13:33:01', '0000-00-00 00:00:00', 2),
(25, '7109010603083860', 58, 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 24, '2019-11-12 14:01:18', '0000-00-00 00:00:00', 1),
(26, '7109010603084143', 82, 'Bowong poso', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 26, '2019-11-12 14:17:06', '0000-00-00 00:00:00', 2),
(27, '7109010603084146', 89, 'Bowong poso', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 26, '2019-11-12 14:29:23', '0000-00-00 00:00:00', 2),
(28, '7109010603084148', 93, 'Bowong poso', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 26, '2019-11-12 14:36:53', '0000-00-00 00:00:00', 2),
(29, '7109010603084154', 96, 'Sawuko', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 26, '2019-11-12 14:47:03', '0000-00-00 00:00:00', 2),
(30, '7109010603084157', 99, 'Lansi', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 26, '2019-11-12 14:56:35', '0000-00-00 00:00:00', 2),
(31, '7109011005170001', 102, 'Tatahadeng', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 27, '2019-11-12 15:09:01', '0000-00-00 00:00:00', 3),
(32, '7109011012120010', 106, 'Tatahadeng', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 27, '2019-11-12 15:20:01', '0000-00-00 00:00:00', 3),
(33, '7109011011160004', 105, 'Tatahadeng', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 27, '2019-11-12 15:21:15', '0000-00-00 00:00:00', 3),
(34, '7109011204130002', 107, 'Tatahadeng', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 27, '2019-11-12 15:31:20', '0000-00-00 00:00:00', 3),
(35, '7109011409160001', 113, 'Tatahadeng', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 28, '2019-11-12 16:07:06', '0000-00-00 00:00:00', 3),
(36, '7109010603084294', 117, 'Basaha', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '002', 29, '2019-11-12 16:23:36', '0000-00-00 00:00:00', 4),
(37, '7109010801110004', 120, 'Basaha', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '002', 29, '2019-11-12 16:33:45', '0000-00-00 00:00:00', 4),
(38, '7109011112120018', 128, 'Basaha', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '002', 30, '2019-11-12 16:53:54', '0000-00-00 00:00:00', 4),
(39, '7109011112120006', 131, 'Basaha', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '002', 30, '2019-11-12 17:02:45', '0000-00-00 00:00:00', 4),
(40, '237590232', 10, 'jshafoihaiohfoieh', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '002', 1, '2019-11-14 05:29:32', '0000-00-00 00:00:00', 1),
(41, '7109010410170017', 136, 'Hekang', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '003', 36, '2019-11-16 02:05:44', '0000-00-00 00:00:00', 5),
(42, '7109010509140004', 140, 'Hekang', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '003', 36, '2019-11-16 02:21:21', '0000-00-00 00:00:00', 5),
(43, '7109010603084277', 143, 'Hekang', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '003', 36, '2019-11-16 02:34:13', '0000-00-00 00:00:00', 5),
(44, '7109010603084278', 145, 'Hekang', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '003', 37, '2019-11-16 02:50:19', '0000-00-00 00:00:00', 5),
(45, '7109010603084281', 148, 'Hekang', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '003', 37, '2019-11-16 02:58:52', '0000-00-00 00:00:00', 5),
(46, '7109010603084280', 150, 'Hekang', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '003', 37, '2019-11-16 03:12:24', '0000-00-00 00:00:00', 5),
(47, '7109010603084286', 152, 'Hekang', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '003', 37, '2019-11-16 03:18:25', '0000-00-00 00:00:00', 5),
(49, '7109011201150001', 157, 'Leuwaha', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '003', 34, '2019-11-20 02:20:43', '0000-00-00 00:00:00', 6),
(50, '7109011112120046', 154, 'Leuwaha', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '003', 34, '2019-11-20 02:22:57', '0000-00-00 00:00:00', 6);

-- --------------------------------------------------------

--
-- Table structure for table `mutasi`
--

CREATE TABLE `mutasi` (
  `id_mutasi` int(11) NOT NULL,
  `nik_mutasi` varchar(16) NOT NULL,
  `nama_mutasi` varchar(45) NOT NULL,
  `tempat_lahir_mutasi` varchar(30) NOT NULL,
  `tanggal_lahir_mutasi` date NOT NULL,
  `jenis_kelamin_mutasi` enum('L','P') NOT NULL,
  `alamat_ktp_mutasi` text NOT NULL,
  `alamat_mutasi` text NOT NULL,
  `desa_kelurahan_mutasi` varchar(30) NOT NULL,
  `kecamatan_mutasi` varchar(30) NOT NULL,
  `kabupaten_kota_mutasi` varchar(30) NOT NULL,
  `provinsi_mutasi` varchar(30) NOT NULL,
  `negara_mutasi` varchar(30) NOT NULL,
  `rt_mutasi` varchar(3) NOT NULL,
  `rw_mutasi` varchar(3) NOT NULL,
  `agama_mutasi` enum('Islam','Kristen','Katholik','Hindu','Budha','Konghucu') NOT NULL,
  `pendidikan_terakhir_mutasi` varchar(20) NOT NULL,
  `pekerjaan_mutasi` varchar(20) NOT NULL,
  `status_perkawinan_mutasi` enum('Kawin','Tidak Kawin') NOT NULL,
  `status_mutasi` enum('Tetap','Kontrak') NOT NULL,
  `id_user` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lingkungan_mutasi` int(3) NOT NULL,
  `keterangan_mutasi` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mutasi`
--

INSERT INTO `mutasi` (`id_mutasi`, `nik_mutasi`, `nama_mutasi`, `tempat_lahir_mutasi`, `tanggal_lahir_mutasi`, `jenis_kelamin_mutasi`, `alamat_ktp_mutasi`, `alamat_mutasi`, `desa_kelurahan_mutasi`, `kecamatan_mutasi`, `kabupaten_kota_mutasi`, `provinsi_mutasi`, `negara_mutasi`, `rt_mutasi`, `rw_mutasi`, `agama_mutasi`, `pendidikan_terakhir_mutasi`, `pekerjaan_mutasi`, `status_perkawinan_mutasi`, `status_mutasi`, `id_user`, `created_at`, `updated_at`, `lingkungan_mutasi`, `keterangan_mutasi`) VALUES
(18, '3173052612890009', 'CHRISTIANTO SALINDEHO', 'Tatahadeng', '1989-12-26', 'L', 'Dalanisang', 'Dalanisang', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 'Kristen', 'S1', 'wiraswasta', 'Kawin', 'Tetap', 34, '2019-11-19 03:06:04', '0000-00-00 00:00:00', 1, 'Meninggal');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `nama_user` varchar(45) NOT NULL,
  `username_user` varchar(20) NOT NULL,
  `password_user` varchar(32) NOT NULL,
  `keterangan_user` text NOT NULL,
  `status_user` enum('Lurah','Staf','RT') NOT NULL,
  `desa_kelurahan_user` varchar(30) NOT NULL,
  `kecamatan_user` varchar(30) NOT NULL,
  `kabupaten_kota_user` varchar(30) NOT NULL,
  `provinsi_user` varchar(30) NOT NULL,
  `negara_user` varchar(30) NOT NULL,
  `rt_user` varchar(3) NOT NULL,
  `rw_user` varchar(3) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lingkungan_user` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `nama_user`, `username_user`, `password_user`, `keterangan_user`, `status_user`, `desa_kelurahan_user`, `kecamatan_user`, `kabupaten_kota_user`, `provinsi_user`, `negara_user`, `rt_user`, `rw_user`, `created_at`, `updated_at`, `lingkungan_user`) VALUES
(1, 'Megawati', 'admin', '1844156d4166d94387f1a4ad031ca5fa', 'admin di aplikasi pendataan warga', '', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '002', '2019-11-14 05:37:23', '2019-11-14 05:37:23', 0),
(23, 'Tam Benyamin', 'rt001', '1434f7533daaf8b0a525ad05f90224f0', 'Rt di lingkungan 1', 'RT', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', '2019-11-12 04:20:02', '0000-00-00 00:00:00', 1),
(24, 'Jeri Mamentu', 'rt002', 'ce97cc611613881987937ee3a24409fb', 'Rt di lingkungan 1', 'RT', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', '2019-11-12 05:24:01', '0000-00-00 00:00:00', 1),
(25, 'RT 003', 'rt003', 'd9bacdfe456641bfd6b52669f8668cf3', 'RT di lingkungan 2', 'RT', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', '2019-11-12 12:35:06', '0000-00-00 00:00:00', 2),
(26, 'RT 004', 'rt004', '42d4c2431154d29be296807ef6537a0b', 'RT di lingkungan 2', 'RT', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', '2019-11-12 13:35:00', '0000-00-00 00:00:00', 2),
(27, 'RT 005', 'rt005', '3ebb8aad717e26405c4c43475f1387bb', 'RT di lingkungan 3', 'RT', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', '2019-11-12 14:59:12', '0000-00-00 00:00:00', 3),
(28, 'RT 006', 'rt006', 'c513e6a9ba65392abb6b2e97c8255ccc', 'RT di lingkungan 3', 'RT', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', '2019-11-12 15:54:44', '0000-00-00 00:00:00', 3),
(29, 'RT 007', 'rt007', '288c9354a1139c568af0d4775d97e824', 'RT di lingkungan 4', 'RT', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '002', '2019-11-12 16:11:39', '0000-00-00 00:00:00', 4),
(30, 'RT 008', 'rt008', '979f1c1cf83d7a0cd0a6ccd9d825d149', 'RT di Lingkungan 4', 'RT', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '002', '2019-11-12 16:46:13', '0000-00-00 00:00:00', 4),
(34, 'Irene Katiandagho', 'staf01', '6d54bc1a06909bd7c320066d44c96dfc', 'Staf di Kelurahan Tatahadeng', 'Staf', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', '2019-11-15 04:10:24', '0000-00-00 00:00:00', 1),
(35, 'Riki Ronald, SE', 'lurah01', 'aa7f1cfe652a2d6d51a3baf9e2a68326', 'Lurah di Kelurahan Tatahadeng', 'Lurah', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', '2019-11-15 04:12:58', '0000-00-00 00:00:00', 1),
(36, 'RT 009', 'rt009', '8c82abdaa5997ec66f581c41fdd65a8e', 'RT di Lingkungan 5', 'RT', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '003', '2019-11-16 01:54:07', '0000-00-00 00:00:00', 5),
(37, 'RT010', 'rt010', 'efe651d8c7160f7f8edf45e08bb41f0c', 'RT di Lingkungan 5', 'RT', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '003', '2019-11-16 02:37:31', '0000-00-00 00:00:00', 5),
(38, 'RT 011', 'rt011', '694b736d5ddb807a3dced5934c91c00b', 'RT di lingkungan 6', 'RT', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '003', '2019-11-16 03:45:01', '0000-00-00 00:00:00', 6),
(39, 'RT 012', 'rt012', '1c34757062cadca8bb6373fdd2800318', 'RT di lingkungan 6', 'RT', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '003', '2019-11-20 02:11:10', '0000-00-00 00:00:00', 6);

-- --------------------------------------------------------

--
-- Table structure for table `warga`
--

CREATE TABLE `warga` (
  `id_warga` int(11) NOT NULL,
  `nik_warga` varchar(16) NOT NULL,
  `nama_warga` varchar(45) NOT NULL,
  `tempat_lahir_warga` varchar(30) NOT NULL,
  `tanggal_lahir_warga` date NOT NULL,
  `jenis_kelamin_warga` enum('L','P') NOT NULL,
  `alamat_ktp_warga` text NOT NULL,
  `alamat_warga` text NOT NULL,
  `desa_kelurahan_warga` varchar(30) NOT NULL,
  `kecamatan_warga` varchar(30) NOT NULL,
  `kabupaten_kota_warga` varchar(30) NOT NULL,
  `provinsi_warga` varchar(30) NOT NULL,
  `negara_warga` varchar(30) NOT NULL,
  `rt_warga` varchar(3) NOT NULL,
  `rw_warga` varchar(3) NOT NULL,
  `agama_warga` enum('Islam','Kristen','Katholik','Hindu','Budha','Konghucu') NOT NULL,
  `pendidikan_terakhir_warga` varchar(20) NOT NULL,
  `pekerjaan_warga` varchar(20) NOT NULL,
  `status_perkawinan_warga` enum('Kawin','Tidak Kawin') NOT NULL,
  `status_warga` enum('Tetap','Kontrak') NOT NULL,
  `id_user` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lingkungan_warga` int(3) NOT NULL,
  `status_kelahiran` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `warga`
--

INSERT INTO `warga` (`id_warga`, `nik_warga`, `nama_warga`, `tempat_lahir_warga`, `tanggal_lahir_warga`, `jenis_kelamin_warga`, `alamat_ktp_warga`, `alamat_warga`, `desa_kelurahan_warga`, `kecamatan_warga`, `kabupaten_kota_warga`, `provinsi_warga`, `negara_warga`, `rt_warga`, `rw_warga`, `agama_warga`, `pendidikan_terakhir_warga`, `pekerjaan_warga`, `status_perkawinan_warga`, `status_warga`, `id_user`, `created_at`, `updated_at`, `lingkungan_warga`, `status_kelahiran`) VALUES
(6, '3173052612890009', 'CHRISTIANTO SALINDEHO', 'Tatahadeng', '1989-12-26', 'L', 'Dalanisang', 'Dalanisang', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 'Kristen', 'S1', 'wiraswasta', 'Kawin', 'Tetap', 23, '2018-01-02 04:26:44', '0000-00-00 00:00:00', 1, ''),
(7, '7109066704830002', 'GRESTIANI IVA DURE', 'SIAU', '1983-04-27', 'P', 'Dalanisang', 'Dalanisang', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 'Kristen', 'SMA', 'IRT', 'Kawin', 'Tetap', 23, '2018-11-12 04:37:27', '0000-00-00 00:00:00', 1, ''),
(8, '7109014708160001', 'IZAURA AMABEL SALINDEHO', 'Sawang', '2016-07-08', 'P', 'Dalanisang', 'Dalanisang', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 'Kristen', 'SD', '', 'Tidak Kawin', 'Tetap', 23, '2018-11-20 11:30:41', '2019-11-12 12:00:57', 1, '1'),
(9, '7171021604890003', 'RANDY BONIFASIUS SALELENG', 'MANADO', '1989-04-16', 'L', 'Dalanisang', 'Dalanisang', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 'Katholik', 'SMA', 'WIRASWASTA', 'Kawin', 'Tetap', 23, '2019-11-12 04:53:20', '0000-00-00 00:00:00', 1, ''),
(10, '7109014908880001', 'ISABELLA AINA ANALAUW', 'AMBON', '1988-08-09', 'P', 'Dalanisang', 'Dalanisang', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 'Katholik', 'S1', 'DOKTER', 'Kawin', 'Tetap', 23, '2019-11-12 04:54:46', '0000-00-00 00:00:00', 1, ''),
(11, '7109010801550003', 'HESKI SALINDEHO', 'TATAHADENG', '1955-08-01', 'L', 'Utasoa', 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 'Kristen', 'SMP', 'PETANI', 'Kawin', 'Tetap', 23, '2019-11-12 05:00:15', '0000-00-00 00:00:00', 1, ''),
(12, '7109015006610004', 'OLITA LUMAKEKI', 'CILACAP', '1961-06-10', 'P', 'Utasoa', 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 'Kristen', 'SMA', 'IRT', 'Kawin', 'Tetap', 23, '2019-11-12 05:01:28', '0000-00-00 00:00:00', 1, ''),
(13, '7109010205760001', 'YOHANIS PREMAN', 'TAHUNA', '1976-02-05', 'L', 'Pelabuhan', 'Pelabuhan', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 'Kristen', 'SMA', 'WIRASWASTA', 'Kawin', 'Tetap', 23, '2019-11-12 05:07:35', '0000-00-00 00:00:00', 1, ''),
(14, '7109017101820003', 'YENIKE KEINTJEM', 'TAGULANDANG', '1982-01-31', 'P', 'Pelabuhan', 'Pelabuhan', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 'Kristen', 'SMA', 'IRT', 'Kawin', 'Tetap', 23, '2019-11-12 05:10:02', '0000-00-00 00:00:00', 1, ''),
(15, '7109016310040001', 'NORLITA SULVIANI PREMAN', 'TAG', '2004-11-23', 'P', 'Pelabuhan', 'Pelabuhan', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 'Kristen', 'SD', 'Siswa', 'Tidak Kawin', 'Tetap', 23, '2019-11-12 05:12:39', '0000-00-00 00:00:00', 1, ''),
(16, '7109016710110001', 'YOLANDA NICKEN PREMAN', 'SIAU', '2011-10-27', 'P', 'Pelabuhan', 'Pelabuhan', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 'Kristen', 'SD', 'Siswa', 'Tidak Kawin', 'Tetap', 23, '2019-11-12 05:18:58', '0000-00-00 00:00:00', 1, ''),
(17, '7109011811870001', 'NOVRIAL BARAKATI', 'TATAHADENG', '1987-11-14', 'L', 'Pelabuhan', 'Pelabuhan', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SMA', 'PNS', 'Kawin', 'Tetap', 24, '2019-11-12 05:36:48', '0000-00-00 00:00:00', 1, ''),
(18, '7109015911900002', 'WINDA HENDRO', 'TATAHADENG', '1990-11-19', 'P', 'Pelabuhan', 'Pelabuhan', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SMA', 'PNS', 'Kawin', 'Tetap', 24, '2019-11-12 05:39:36', '0000-00-00 00:00:00', 1, ''),
(19, '7109010509920001', 'EVANDER WAHYUDI GUNENA', 'LIA', '1992-09-05', 'L', 'Utasoa', 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'S1', 'Honor', 'Kawin', 'Tetap', 24, '2019-11-12 06:45:51', '0000-00-00 00:00:00', 1, ''),
(20, '7102136604940002', 'PRISKA PRISKILA NUNUMETE', 'JAKARTA', '1994-04-26', 'P', 'Utasoa', 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'S1', 'IRT', 'Kawin', 'Tetap', 24, '2019-11-12 06:47:48', '0000-00-00 00:00:00', 1, ''),
(21, '7109016607140001', 'EVANGELIKA AQUINA GUNENA', 'MANADO', '2014-07-26', 'P', 'Utasoa', 'Utaso', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SD', 'Siswa', 'Tidak Kawin', 'Tetap', 24, '2019-11-12 06:49:37', '0000-00-00 00:00:00', 1, ''),
(22, '7371112503860001', 'WILMAR LABAENG LUKAS', 'UJUNG PANDANG', '1986-03-25', 'L', 'Lewae', 'Lewae', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SMA', 'Pelaut', 'Kawin', 'Tetap', 24, '2018-11-12 07:23:09', '0000-00-00 00:00:00', 1, ''),
(23, '7109016706930002', 'INDIRA JACOBUS', 'ULU SIAU', '1993-06-27', 'P', 'Lewae', 'Lewae', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'D3', 'Perawat', 'Kawin', 'Tetap', 24, '2018-11-12 07:25:11', '0000-00-00 00:00:00', 1, ''),
(24, '7109010905510001', 'TOMIX MERPATI', 'BUYAT', '1951-05-09', 'L', 'Utasoa', 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SMA', 'Petani', 'Kawin', 'Tetap', 24, '2018-11-12 07:32:41', '0000-00-00 00:00:00', 1, ''),
(25, '7109015009680001', 'TELDA JANEKE TAMAKA', 'SANGIHE TALAUD', '1968-09-10', 'P', 'Utasoa', 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'S1', 'Guru', 'Kawin', 'Tetap', 24, '2019-11-12 07:35:41', '0000-00-00 00:00:00', 1, ''),
(26, '7109013005960001', 'TEMIKS MERPATI', 'ULU SIAU', '1996-05-30', 'L', 'Utasoa', 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'S1', 'Guru', 'Tidak Kawin', 'Tetap', 24, '2019-11-12 07:37:48', '0000-00-00 00:00:00', 1, ''),
(27, '7109012003990001', 'IRVIN MERPATI', 'ULU SIAU', '1999-03-20', 'L', 'Utasoa', 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SMA', 'Polisi', 'Tidak Kawin', 'Tetap', 24, '2019-11-12 07:40:03', '0000-00-00 00:00:00', 1, ''),
(28, '7109011401720001', 'JEMMY CLLIEF DANDEL', 'LANGOWAN', '1972-01-14', 'L', 'Utasoa', 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SMP', 'Pedagang', 'Kawin', 'Tetap', 24, '2019-11-12 07:43:47', '0000-00-00 00:00:00', 1, ''),
(29, '7109014707780001', 'FERA KAKALANG', 'APELAWO', '1978-07-07', 'P', 'Utasoa', 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SMP', 'IRT', 'Kawin', 'Tetap', 24, '2019-11-12 07:45:26', '0000-00-00 00:00:00', 1, ''),
(30, '7109011706000001', 'RIVALDO DANDEL', 'LANGOWAN', '2000-06-17', 'L', 'Utasoa', 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SMA', 'Mahasiswa', 'Tidak Kawin', 'Tetap', 24, '2019-11-12 07:47:08', '0000-00-00 00:00:00', 1, ''),
(31, '7109010109540002', 'YAN ANDRIS MANGANGAWE', 'TATAHADENG', '1954-09-01', 'L', 'Utasoa', 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SMP', 'Petani', 'Tidak Kawin', 'Tetap', 24, '2019-11-12 08:16:41', '0000-00-00 00:00:00', 1, ''),
(32, '7109015610900001', 'IRENE MANGANGAWE', 'TATAHADENG', '1990-10-16', 'P', 'Utasoa', 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SMA', 'IRT', 'Tidak Kawin', 'Tetap', 24, '2019-11-12 08:18:55', '0000-00-00 00:00:00', 1, ''),
(33, '7109015412140001', 'ANGELI DESTIA MANGANGAWE', 'TATAHADENG', '2014-12-14', 'P', 'Utasoa', 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'Tidak Sekolah', '', 'Tidak Kawin', 'Tetap', 24, '2019-11-12 08:20:36', '0000-00-00 00:00:00', 1, ''),
(34, '7109011108590002', 'RONY MANGANGAWE', 'SIAU', '1959-08-11', 'L', 'Utasoa', 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SMP', 'Petani', 'Kawin', 'Tetap', 24, '2019-11-12 08:26:58', '0000-00-00 00:00:00', 1, ''),
(35, '7109016102670001', 'ANDRED ANDALANGI', 'LIRUNG', '1967-02-21', 'P', 'Utasoa', 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'D3', 'Guru', 'Kawin', 'Tetap', 24, '2019-11-12 08:28:25', '0000-00-00 00:00:00', 1, ''),
(36, '7109012407030001', 'YOSUA MANGANGAWE', 'SIAU', '2003-07-24', 'L', 'Utasoa', 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SMP', 'Siswa', 'Tidak Kawin', 'Tetap', 24, '2019-11-12 08:30:11', '0000-00-00 00:00:00', 1, ''),
(37, '7109010311930002', 'NEPLIS MANGANGAWE', 'LIRUNG', '1993-11-03', 'L', 'Utasoa', 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SMA', 'Mahasiswa', 'Tidak Kawin', 'Tetap', 24, '2019-11-12 08:31:46', '0000-00-00 00:00:00', 1, ''),
(38, '7109015010580001', 'HENNY ELIZABETH SAMBENTHIRO', 'TATAHADENG', '1958-10-10', 'P', 'Utasoa', 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Katholik', 'SMP', 'Wiraswasta', 'Kawin', 'Tetap', 24, '2019-11-12 08:36:20', '0000-00-00 00:00:00', 1, ''),
(39, '7109012707860001', 'PASKALIS JENLY ADAM', 'ULU SIAU', '1987-07-27', 'L', 'Utasoa', 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Katholik', 'SMA', 'Pelaut', 'Tidak Kawin', 'Tetap', 24, '2019-11-12 08:37:47', '0000-00-00 00:00:00', 1, ''),
(40, '7109011602600001', 'SAN KAPAHESE', 'ULU SIAU', '1960-02-16', 'L', 'Utasoa', 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Katholik', 'SMA', 'Petani', 'Kawin', 'Tetap', 24, '2019-11-12 08:42:25', '0000-00-00 00:00:00', 1, ''),
(41, '7109016103660001', 'MARISSTELA MANDEROS', 'ULU SIAU', '1966-03-21', 'P', 'Utasoa', 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Katholik', 'S1', 'PNS', 'Kawin', 'Tetap', 24, '2019-11-12 08:44:00', '0000-00-00 00:00:00', 1, ''),
(42, '7109016105940001', 'BRIGITTA MELANIA KAPAHESE', 'ULU SIAU', '1994-05-21', 'P', 'Utasoa', 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Katholik', 'S1', '', 'Tidak Kawin', 'Tetap', 24, '2019-11-12 08:45:41', '0000-00-00 00:00:00', 1, ''),
(43, '7109014810390001', 'MARIE AUGUSTIEN PANGEMANAN', 'TOMOHON', '1939-10-08', 'P', 'Utasoa', 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Katholik', 'SMA', '', 'Tidak Kawin', 'Tetap', 24, '2019-11-12 08:47:06', '0000-00-00 00:00:00', 1, ''),
(44, '7109010907580001', 'BRIXIUS SALINDEHO', 'TATAHADENG', '1958-07-09', 'L', 'Utasoa', 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SMA', 'Petani', 'Kawin', 'Tetap', 24, '2019-11-12 08:54:17', '0000-00-00 00:00:00', 1, ''),
(45, '7109014112630001', 'IVONE KALANGIT', 'TATAHADENG', '1963-12-01', 'P', 'Utasoa', 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SMA', 'IRT', 'Kawin', 'Tetap', 24, '2019-11-12 08:55:29', '0000-00-00 00:00:00', 1, ''),
(46, '7109012207970001', 'ANDHIKA JULIO PRAWIRO SALINDEHO', 'JAKARTA', '1997-07-22', 'L', 'Utasoa', 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SMA', 'Mahasiswa', 'Tidak Kawin', 'Tetap', 24, '2019-11-12 08:58:16', '0000-00-00 00:00:00', 1, ''),
(47, '7109010603600001', 'SONNY SALINDEHO', 'ULU SIAU', '1960-03-06', 'L', 'Utasoa', 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SMA', 'Petani', 'Kawin', 'Tetap', 24, '2019-11-12 09:02:28', '0000-00-00 00:00:00', 1, ''),
(48, '7109016102640001', 'FRAULAINE FAM SIEW LEEN', 'SINGAPURA', '1964-02-21', 'P', 'Utasoa', 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SMA', 'Wiraswasta', 'Kawin', 'Tetap', 24, '2019-11-12 09:05:05', '0000-00-00 00:00:00', 1, ''),
(49, '7109015006980001', 'VIRA ANGELINA SALINDEHO', 'ULU SIAU', '1998-06-10', 'P', 'Utasoa', 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SMA', 'Mahasiswa', 'Tidak Kawin', 'Tetap', 24, '2019-11-12 09:06:57', '0000-00-00 00:00:00', 1, ''),
(50, '7109010112410001', 'FRANSISKUS DJAJAPRANATA', 'MANADO', '1941-11-16', 'L', 'Utasoa', 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Katholik', 'SMA', 'Wiraswasta', 'Kawin', 'Tetap', 24, '2019-11-12 09:10:35', '0000-00-00 00:00:00', 1, ''),
(51, '7109014906460001', 'REGINA DAVID', 'TARORANE', '1948-12-01', 'P', 'Utasoa\r\n', 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Katholik', 'SMA', 'Wiraswasta', 'Kawin', 'Tetap', 24, '2019-11-12 09:11:50', '0000-00-00 00:00:00', 1, ''),
(52, '7109011802720001', 'HERDY KAPAHESE', 'TATAHADENG', '1976-02-19', 'L', 'Utasoa', 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SMA', 'Petani', 'Kawin', 'Tetap', 24, '2019-11-12 09:14:53', '0000-00-00 00:00:00', 1, ''),
(53, '7109016701720001', 'DEBBY ELISABETH SALINDEHO', 'TATAHADENG', '1972-01-27', 'P', 'Utasoa', 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SMA', 'IRT', 'Kawin', 'Tetap', 24, '2019-11-12 09:16:03', '0000-00-00 00:00:00', 1, ''),
(54, '7109011911020001', 'ALESSANDRO GILBERT KAPAHESE', 'TAHUNA', '2002-11-19', 'L', 'Utasoa', 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SMP', 'Siswa', 'Tidak Kawin', 'Tetap', 24, '2018-11-12 09:17:24', '0000-00-00 00:00:00', 1, ''),
(55, '7109012401390001', 'JOUTJE.O. MANGOLO', 'TATAHADENG', '1939-01-24', 'L', 'Utasoa', 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 'Kristen', 'SMA', 'Pendeta', 'Kawin', 'Tetap', 23, '2019-11-12 10:16:21', '0000-00-00 00:00:00', 1, ''),
(56, '7109012007100001', 'ANUGRAH DEANDRY KAPAHESE', 'MANADO', '2010-07-20', 'L', 'Utasoa', 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SD', 'Siswa', 'Tidak Kawin', 'Tetap', 24, '2019-11-12 10:33:22', '0000-00-00 00:00:00', 1, ''),
(57, '7109015803940001', 'OKIE MARIANA CLAUDIA SALINDEHO', 'JAKARTA', '1994-03-17', 'P', 'Utasoa', 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'S1', '', 'Tidak Kawin', 'Tetap', 24, '2019-11-12 10:34:41', '0000-00-00 00:00:00', 1, ''),
(58, '7109012610480001', 'IGNATIUS SALINDEHO', 'TARORANE', '1948-10-26', 'L', 'Utasoa', 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SMA', 'Petani', 'Kawin', 'Tetap', 24, '2019-11-12 10:37:16', '0000-00-00 00:00:00', 1, ''),
(59, '7109014104500001', 'GERTJI MANGERONGKONDA', 'MANADO', '1950-04-01', 'P', 'Utasoa', 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SMA', 'Wiraswasta', 'Kawin', 'Tetap', 24, '2019-11-12 10:39:00', '0000-00-00 00:00:00', 1, ''),
(60, '710901530980002', 'MEGAWATI SALINDEHO', 'JAKARTA', '1999-09-13', 'P', 'Utasoa', 'Utasoa', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SMA', 'Mahasiswa', 'Tidak Kawin', 'Tetap', 24, '2019-11-12 10:40:29', '0000-00-00 00:00:00', 1, ''),
(62, '7109016006740002', 'WILHELMINA BULELE', 'TALAUD', '1974-06-20', 'P', 'lansi', 'lansi', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 'Kristen', 'SMA', 'IRT', 'Kawin', 'Tetap', 19, '2019-11-12 12:03:36', '0000-00-00 00:00:00', 1, ''),
(68, '7109012610640001', 'FRETS DEREK', 'ULU', '1964-09-26', 'L', 'Lansi', 'Lansi', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 'Kristen', 'SMP', 'Petani', 'Kawin', 'Tetap', 25, '2018-11-12 12:48:52', '0000-00-00 00:00:00', 2, ''),
(69, '7109016006740002', 'WILHELMINA BULELE', 'TALAUD', '1974-06-20', 'P', 'Lansia', 'Lansia', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 'Kristen', 'SMA', 'IRT', 'Kawin', 'Tetap', 25, '2018-11-12 13:00:05', '0000-00-00 00:00:00', 2, ''),
(70, '7109015207950001', 'CHYNTYA DEREK', 'TALAUD', '1995-07-12', 'P', 'Lansi', 'Lansi', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 'Kristen', 'SMA', 'Mahasiswa', 'Tidak Kawin', 'Tetap', 25, '2018-11-12 13:01:43', '0000-00-00 00:00:00', 2, ''),
(71, '7109011706010001', 'JEREMY INDRA DEREK', 'TALAUD', '2001-06-17', 'L', 'Lansi', 'Lansi', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 'Kristen', 'SMA', 'Mahasiswa', 'Tidak Kawin', 'Tetap', 25, '2019-11-12 13:03:08', '0000-00-00 00:00:00', 2, ''),
(72, '7109014408030001', 'ANGEL AGATHA DEREK', 'TATAHADENG', '2003-08-04', 'P', 'Lansi', 'Lansi', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 'Kristen', 'SMP', 'Siswa', 'Tidak Kawin', 'Tetap', 25, '2019-11-12 13:04:27', '0000-00-00 00:00:00', 2, ''),
(73, '7109014405060002', 'MATTEW ANDRE DEREK', 'TATAHADENG', '2004-05-20', 'L', 'Lansi', 'Lansi', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 'Kristen', 'SD', 'Siswa', 'Tidak Kawin', 'Tetap', 25, '2019-11-12 13:05:39', '0000-00-00 00:00:00', 2, ''),
(74, '7109016301670001', 'YANTJILINE SALINDEHO', 'TATAHADENG', '1967-01-23', 'P', 'Tatahadeng', 'Tatahadeng', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 'Kristen', 'SMA', 'IRT', 'Tidak Kawin', 'Tetap', 25, '2019-11-12 13:10:06', '0000-00-00 00:00:00', 2, ''),
(75, '7109015708030001', 'CHRISENSIA ANDARA SALINDEHO', 'JAYAPURA', '2003-08-17', 'P', 'Tatahadeng', 'Tatahadeng', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 'Kristen', 'SMP', 'Siswa', 'Tidak Kawin', 'Tetap', 25, '2019-11-12 13:11:35', '0000-00-00 00:00:00', 2, ''),
(76, '7109010904640002', 'VERISON BOLY', 'KANANG', '1964-04-09', 'L', 'Poso kadio', 'Poso kadio', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 'Kristen', 'SMA', 'Wiraswasta', 'Kawin', 'Tetap', 25, '2019-11-12 13:16:38', '0000-00-00 00:00:00', 2, ''),
(77, '71090160066 3000', 'SARAH SUNDANA', 'TATAHADENG', '1963-06-20', 'P', 'Poso kedio', 'Poso kedio', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 'Kristen', 'SMA', 'IRT', 'Kawin', 'Tetap', 25, '2019-11-12 13:19:11', '0000-00-00 00:00:00', 2, ''),
(78, '7109016112890002', 'RIAWATY BOLY', 'TATAHADENG', '1989-12-21', 'P', 'Poso kedio', 'poso kedio', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 'Kristen', 'SMA', 'Mahasiswa', 'Tidak Kawin', 'Tetap', 25, '2019-11-12 13:21:26', '0000-00-00 00:00:00', 2, ''),
(79, '7109014205970001', 'DEWI SITANIA BOLY', 'TAHUNA', '1997-05-02', 'P', 'Poso kedio', 'Poso kedio', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 'Kristen', 'SMA', 'Mahasiswa', 'Tidak Kawin', 'Tetap', 25, '2019-11-12 13:25:03', '0000-00-00 00:00:00', 2, ''),
(80, '7109010212650001', 'BIN KABUHUNG', 'TATAHADENG', '1965-12-02', 'L', 'poso', 'poso', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 'Kristen', 'SMA', 'kostor', 'Kawin', 'Tetap', 25, '2019-11-12 13:30:41', '0000-00-00 00:00:00', 2, ''),
(81, '7109016909670002', 'SIRTJE KATULUNG', 'TATAHADENG', '1967-09-29', 'P', 'Poso', 'Poso', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 'Kristen', 'SMA', 'IRT', 'Kawin', 'Tetap', 25, '2019-11-12 13:32:17', '0000-00-00 00:00:00', 2, ''),
(82, '7109012307700001', 'RUDY DARAMU', 'TATAHADENG', '1970-07-23', 'L', 'Bowong poso', 'Bowong poso', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SMA', 'Petani', 'Kawin', 'Tetap', 26, '2019-11-12 14:06:55', '0000-00-00 00:00:00', 2, ''),
(83, '7109016011690002', 'STELA MOSE', 'BAHU', '1969-11-20', 'P', 'Bowong poso', 'Bowong poso', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SMA', 'IRT', 'Kawin', 'Tetap', 26, '2019-11-12 14:08:27', '0000-00-00 00:00:00', 2, ''),
(84, '7109010906950001', 'ROYKE DAVID DARAMU', 'MANADO', '1995-06-09', 'L', 'Bowong poso', 'Bowong poso', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SMA', 'Mahasiswa', 'Tidak Kawin', 'Tetap', 26, '2019-11-12 14:09:57', '0000-00-00 00:00:00', 2, ''),
(85, '7109011108950002', 'RAINALDY DARAMU', 'BAHU', '1997-07-16', 'L', 'Bowong poso', 'Bowong poso', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SMA', 'Mahasiswa', 'Tidak Kawin', 'Tetap', 26, '2019-11-12 14:11:43', '0000-00-00 00:00:00', 2, ''),
(86, '7109010101990001', 'RIO JANWAR DARAMU', 'TATAHADENG', '1999-01-01', 'L', 'Bowong poso', 'Bowong poso', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SMA', 'Mahasiswa', 'Tidak Kawin', 'Tetap', 26, '2019-11-12 14:13:07', '0000-00-00 00:00:00', 2, ''),
(87, '7109014606050001', 'JENNI ERIKA DARAMU', 'ULU SIAU', '2005-06-06', 'P', 'Bowong poso', 'Bowong poso', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SD', 'Siswa', 'Tidak Kawin', 'Tetap', 26, '2019-11-12 14:14:39', '0000-00-00 00:00:00', 2, ''),
(88, '7109014805100001', 'MEISYA. VISCHA. DARAMU', 'TATAHADENG', '2010-05-08', 'P', 'Bowong poso', 'Bowong poso', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SD', 'Siswa', 'Tidak Kawin', 'Tetap', 26, '2019-11-12 14:16:24', '0000-00-00 00:00:00', 2, ''),
(89, '7109010209760001', 'STENLY MARTHIN', 'TATAHADENG', '1976-09-02', 'L', 'Bowong poso', 'Bowong poso', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'S1', 'PNS', 'Kawin', 'Tetap', 26, '2019-11-12 14:21:19', '0000-00-00 00:00:00', 2, ''),
(90, '7109014510850001', 'LILIANTY GAGHUNTING', 'BAHU', '1985-10-05', 'P', 'Bowong poso', 'Bowong poso', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SMA', 'IRT', 'Kawin', 'Tetap', 26, '2019-11-12 14:22:47', '0000-00-00 00:00:00', 2, ''),
(91, '7109016411050001', 'BUNGA ANANDA MARTHIN', 'TATAHADENG', '2005-11-24', 'P', 'Bowong poso', 'Bowong poso', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SD', 'Siswa', 'Tidak Kawin', 'Tetap', 26, '2019-11-12 14:26:24', '0000-00-00 00:00:00', 2, ''),
(92, '7109016601120002', 'EUGENIA DWI MARTHIN', 'ULU SIAU', '2012-01-26', 'P', 'Bowong poso', 'Bowong poso', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SD', 'Siswa', 'Tidak Kawin', 'Tetap', 26, '2019-11-12 14:28:32', '0000-00-00 00:00:00', 2, ''),
(93, '7109012607700001', 'JONDRIS KAMURAHANG', 'TATAHADENG', '1970-07-26', 'L', 'Bowong poso', 'Bowong poso', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SMA', 'Sopir', 'Kawin', 'Tetap', 26, '2019-11-12 14:33:01', '0000-00-00 00:00:00', 2, ''),
(94, '7109016402710001', 'ELISABETH VAUSIANE PANGGURUANG', 'PAGHULU', '1971-02-24', 'P', 'Bowong poso', 'Bowong poso', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SMA', 'IRT', 'Kawin', 'Tetap', 26, '2019-11-12 14:34:39', '0000-00-00 00:00:00', 2, ''),
(95, '7109015610980002', 'JELISA VIVIANI KAMURAHANG', 'PAGHULU', '1998-10-16', 'P', 'Bowong poso', 'Bowong poso', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SMA', 'Mahasiswa', 'Tidak Kawin', 'Tetap', 26, '2019-11-12 14:36:07', '0000-00-00 00:00:00', 2, ''),
(96, '7109011511550001', 'JAP TUMBIO', 'TATAHADENG', '1955-11-15', 'L', 'Sawuko', 'Sawuko', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SMP', 'Tukang', 'Kawin', 'Tetap', 26, '2019-11-12 14:41:55', '0000-00-00 00:00:00', 2, ''),
(97, '7109014409570001', 'NET GOHA', 'TATAHADENG', '1957-09-04', 'P', 'Sawuko', 'Sawuko', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SMP', 'IRT', 'Kawin', 'Tetap', 26, '2019-11-12 14:43:20', '0000-00-00 00:00:00', 2, ''),
(98, '7109016806840001', 'IMELDA TUMBIO', 'TATAHADENG', '1984-06-28', 'P', 'Sawuko', 'Sawuko', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SMA', '', 'Tidak Kawin', 'Tetap', 26, '2019-11-12 14:46:00', '0000-00-00 00:00:00', 2, ''),
(99, '7109010503580001', 'MEZAK TUMBIO', 'TATAHADENG', '1958-03-05', 'L', 'Lansi', 'Lansi', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SMP', 'Tukang', 'Kawin', 'Tetap', 26, '2019-11-12 14:50:53', '0000-00-00 00:00:00', 2, ''),
(100, '7109014403670001', 'YORINO MANANEKE', 'BUHA', '1967-03-04', 'P', 'Lansi', 'Lansi', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SMA', 'IRT', 'Kawin', 'Tetap', 26, '2019-11-12 14:52:48', '0000-00-00 00:00:00', 2, ''),
(101, '7109013112920002', 'JOSEPH TUMBIO', 'TATAHADENG', '1992-12-31', 'L', 'Lansi', 'Lansi', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SMA', '', 'Tidak Kawin', 'Tetap', 26, '2019-11-12 14:55:53', '0000-00-00 00:00:00', 2, ''),
(102, '7109016303790002', 'AFNIATI WASNI TAGHULIHI', 'ULU SIAU', '1979-03-23', 'P', 'Tatahadeng', 'Tatahadeng', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 'Kristen', 'S1', 'Wiraswasta', 'Tidak Kawin', 'Tetap', 27, '2018-11-12 15:04:16', '0000-00-00 00:00:00', 3, ''),
(103, '7109015304090001', 'TYASKA LITHANIA PUTRI LAWERE', 'TATAHADENG', '2009-04-13', 'P', 'Tatahadeng', 'Tatahadeng', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 'Kristen', 'SD', 'Siswa', 'Tidak Kawin', 'Tetap', 27, '2018-11-12 15:06:04', '0000-00-00 00:00:00', 3, ''),
(104, '7109011211110001', 'BRANDON AGRA DWI PUTRA LAWERE', 'BITUNG', '2011-11-12', 'L', 'Tatahadeng', 'Tatahadeng', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 'Kristen', 'SD', 'Siswa', 'Tidak Kawin', 'Tetap', 27, '2018-11-12 15:08:00', '0000-00-00 00:00:00', 3, ''),
(105, '7109012304750001', 'SONNY BAWOLE', 'TATAHADENG', '1975-04-23', 'L', 'Tatahadeng', 'Tatahadeng', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 'Kristen', 'SMA', '', 'Tidak Kawin', 'Tetap', 27, '2018-11-12 15:11:54', '0000-00-00 00:00:00', 3, ''),
(106, '7109012505750002', 'ROMANTIKA DAVID', 'TATAHADENG', '1975-05-25', 'L', 'Bowong poso', 'Bowong poso', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 'Kristen', 'SMA', 'Wiraswasta', 'Tidak Kawin', 'Tetap', 27, '2018-11-12 15:19:06', '0000-00-00 00:00:00', 3, ''),
(107, '7109016606780001', 'BERNI MAMEWE', 'TATAHADENG', '1978-06-26', 'L', 'Tatahadeng', 'Tatahadeng', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 'Kristen', 'SD', 'Petani', 'Tidak Kawin', 'Tetap', 27, '2018-11-12 15:25:17', '0000-00-00 00:00:00', 3, ''),
(108, '7109011001070001', 'JOVANO MARCELLO MAMEWE', 'TATAHADENG', '2007-01-10', 'L', 'Tatahadeng', 'Tatahadeng', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 'Kristen', 'SD', 'Siswa', 'Tidak Kawin', 'Tetap', 27, '2019-11-12 15:26:59', '0000-00-00 00:00:00', 3, ''),
(109, '7109012706100001', 'JUNIKO PRATAMA PODA', 'TATAHADENG', '2010-06-27', 'L', 'Tatahadeng', 'Tatahadeng', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 'Kristen', 'SD', 'Siswa', 'Tidak Kawin', 'Tetap', 27, '2019-11-12 15:28:54', '0000-00-00 00:00:00', 3, ''),
(110, '7109011712550002', 'BEN MAMEWE', 'TATAHADENG', '1955-12-17', 'L', 'Tatahadeng', 'Tatahadeng', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 'Kristen', 'SD', 'Petani', 'Tidak Kawin', 'Tetap', 27, '2019-11-12 15:30:35', '0000-00-00 00:00:00', 3, ''),
(111, '7172081603900001', 'MARKUS KIRIMAN', 'BATURAPA', '1990-03-16', 'L', 'Tatahadeng', 'Tatahadeng', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 'Kristen', 'SMA', 'Petani', 'Kawin', 'Tetap', 27, '2019-11-12 15:50:44', '0000-00-00 00:00:00', 3, ''),
(112, '7109014303860001', 'ANSELA JACOBUS', 'TATAHADENG', '1986-03-03', 'P', 'Tatahadeng', 'Tatahadeng', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 'Kristen', 'SMA', 'IRT', 'Kawin', 'Tetap', 27, '2019-11-12 15:52:35', '0000-00-00 00:00:00', 3, ''),
(113, '7109014310650001', 'THERESIA MALOHING', 'BITUNG', '1965-10-03', 'P', 'Tatahadeng', 'Tatahadeng', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SMA', 'IRT', 'Kawin', 'Tetap', 28, '2019-11-12 15:58:52', '0000-00-00 00:00:00', 3, ''),
(114, '7109012811960001', 'RIVO DANNY BAWANDA', 'SORONG', '1996-11-28', 'L', 'Tatahadeng', 'Tatahadeng', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SMA', 'Pelaut', 'Tidak Kawin', 'Tetap', 28, '2019-11-12 16:00:59', '0000-00-00 00:00:00', 3, ''),
(115, '7109016004000002', 'STIEN MAGDALENA BAWANDA', 'PALU', '2000-04-02', 'P', 'Tatahadeng', 'Tatahadeng', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SMA', 'Mahasiswa', 'Tidak Kawin', 'Tetap', 28, '2019-11-12 16:04:07', '0000-00-00 00:00:00', 3, ''),
(116, '7109014405050002', 'MEIDY BAWANDA', 'TATAHADENG', '2005-05-04', 'P', 'Tatahadeng', 'Tatahadeng', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '001', 'Kristen', 'SMP', 'Siswa', 'Tidak Kawin', 'Tetap', 28, '2019-11-12 16:06:05', '0000-00-00 00:00:00', 3, ''),
(117, '7109012403630001', 'HENDRIK BAWOLE', 'TATAHADENG', '1963-03-24', 'L', 'Basaha', 'Basaha', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '002', 'Kristen', 'SD', 'Petani', 'Kawin', 'Tetap', 29, '2018-11-12 16:18:36', '0000-00-00 00:00:00', 4, ''),
(118, '7109015512670002', 'TUTLIN TAPAHING', 'MAKALEHI', '2015-02-19', 'P', 'Basaha', 'Basaha', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '002', 'Kristen', 'SD', 'IRT', 'Kawin', 'Tetap', 29, '2018-11-12 16:20:13', '0000-00-00 00:00:00', 4, ''),
(119, '7109016104010001', 'ASTRIA S BAWOLE', 'TATAHADENG', '2001-04-21', 'P', 'Basaha', 'Basaha', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '002', 'Kristen', 'SMP', 'Siswa', 'Tidak Kawin', 'Tetap', 29, '2018-11-12 16:22:36', '0000-00-00 00:00:00', 4, ''),
(120, '7109011207700001', 'TERRY DART HAMBARI', 'TATAHADENG', '1970-07-12', 'L', 'Basaha', 'Basaha', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '002', 'Kristen', 'SMA', 'Petani', 'Kawin', 'Tetap', 29, '2018-11-12 16:28:34', '0000-00-00 00:00:00', 4, ''),
(121, '7109017009720003', 'SYANET GANDARIA', 'TATAHADENG', '1972-09-30', 'P', 'Basaha', 'Basaha', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '002', 'Kristen', 'SMA', 'IRT', 'Kawin', 'Tetap', 29, '2018-11-12 16:30:07', '0000-00-00 00:00:00', 4, ''),
(122, '7109011201930001', 'ENRICO JANUARDY HAMBARI', 'SIAU', '1993-01-12', 'L', 'Basaha', 'Basaha', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '002', 'Kristen', 'S1', '', 'Tidak Kawin', 'Tetap', 29, '2019-11-12 16:31:37', '0000-00-00 00:00:00', 4, ''),
(123, '7109014801050002', 'EUODIA GRACELLA HAMBARI', 'TATAHADENG', '2005-01-08', 'P', 'Basaha', 'Basaha', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '002', 'Kristen', 'SMP', 'Siswa', 'Tidak Kawin', 'Tetap', 29, '2019-11-12 16:33:06', '0000-00-00 00:00:00', 4, ''),
(124, '7109010606760002', 'WOLTER HENDRA MIRONTONENG', 'TATAHADENG', '1976-06-06', 'L', 'Basaha', 'Basaha', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '002', 'Islam', 'SMA', 'Petani', 'Kawin', 'Tetap', 29, '2019-11-12 16:37:50', '0000-00-00 00:00:00', 4, ''),
(125, '7109016304830001', 'TRY SULISTIA NINGSIH', 'JAKARTA', '1983-04-23', 'P', 'Basaha', 'Basaha', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '002', 'Islam', 'SMA', 'IRT', 'Kawin', 'Tetap', 29, '2019-11-12 16:39:39', '0000-00-00 00:00:00', 4, ''),
(126, '7109014105060001', 'WINDY TIARA SAPUTRI MIRONTONENG', 'BANTEN', '2006-05-01', 'P', 'Basaha', 'Basaha', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '002', 'Islam', 'SMP', 'Siswa', 'Tidak Kawin', 'Tetap', 29, '2019-11-12 16:42:15', '0000-00-00 00:00:00', 4, ''),
(127, '7109013005100001', 'MOHAMAD.R.A.MIRONTONENG', 'TATAHADENG', '2010-05-30', 'L', 'Basaha', 'Basaha', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '002', 'Islam', 'SD', 'Siswa', 'Tidak Kawin', 'Tetap', 29, '2019-11-12 16:44:01', '0000-00-00 00:00:00', 4, ''),
(128, '7109011802820001', 'ALFRED TATIPANG', 'TATAHADENG', '1982-02-18', 'L', 'Basaha', 'Basaha', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '002', 'Kristen', 'SD', 'Petani', 'Kawin', 'Tetap', 30, '2019-11-12 16:49:52', '0000-00-00 00:00:00', 4, ''),
(129, '7109015207800001', 'YULIKE KAUMBUR', 'TATAHADENG', '1980-07-12', 'P', 'Basaha', 'Basaha', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '002', 'Kristen', 'SMA', 'IRT', 'Kawin', 'Tetap', 30, '2019-11-12 16:51:32', '0000-00-00 00:00:00', 4, ''),
(130, '7109017112090001', 'MAISYA LAURENSWARY TATIPANG', 'TATAHADENG', '2009-12-31', 'P', 'Basaha', 'Basaha', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '002', 'Kristen', 'SD', 'Siswa', 'Tidak Kawin', 'Tetap', 30, '2019-11-12 16:53:13', '0000-00-00 00:00:00', 4, ''),
(131, '7109014201560002', 'LELI MOKODOMPIS', 'SIAU', '1956-01-02', 'P', 'Basaha', 'Basaha', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '002', 'Kristen', 'SD', 'IRT', 'Tidak Kawin', 'Tetap', 30, '2019-11-12 16:58:36', '0000-00-00 00:00:00', 4, ''),
(132, '7109014505030001', 'LAURIN MEISTASYE KALANGIT', 'MANADO', '2003-05-05', 'P', 'Basaha', 'Basaha', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '002', 'Kristen', 'SMP', 'Siswa', 'Tidak Kawin', 'Tetap', 30, '2019-11-12 17:00:21', '0000-00-00 00:00:00', 4, ''),
(133, '7109014404110002', 'SANDRA APRILISYE KALANGIT', 'BASAHA', '2011-04-04', 'P', 'Basaha', 'Basaha', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '002', 'Kristen', 'SD', 'Siswa', 'Tidak Kawin', 'Tetap', 30, '2019-11-12 17:02:00', '0000-00-00 00:00:00', 4, ''),
(136, '7109010212860005', 'RUSLAN ABAS', 'MANADO', '1986-02-12', 'L', 'Hekang', 'Hekang', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '003', 'Islam', 'SMA', 'Pedagang', 'Kawin', 'Tetap', 36, '2018-11-16 01:59:07', '0000-00-00 00:00:00', 5, ''),
(137, '7109014802850006', 'DIAN KAMURAHAN', 'TARORANE', '1986-08-02', 'P', 'Hekang', 'Hekang', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '003', 'Islam', 'SMA', 'IRT', 'Kawin', 'Tetap', 36, '2018-11-16 02:01:10', '0000-00-00 00:00:00', 5, ''),
(138, '7109012404060001', 'PUTRA INDRA ABAS', 'TARORANE', '2006-04-24', 'L', 'Hekang', 'Hekang', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '003', 'Islam', 'SD', 'Siswa', 'Tidak Kawin', 'Tetap', 36, '2018-11-16 02:03:10', '0000-00-00 00:00:00', 5, ''),
(139, '7109014810160001', 'PUTRI NADHIRA ABAS', 'TATAHADENG', '2016-10-08', 'P', 'Hekang', 'Hekang', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '003', 'Islam', 'Tidak Sekolah', '', 'Tidak Kawin', 'Tetap', 36, '2018-11-16 02:04:46', '0000-00-00 00:00:00', 5, ''),
(140, '7109011411940001', 'ROMI TAMPILANG', 'TATAHADENG', '1994-11-14', 'L', 'Hekang', 'Hekang', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '003', 'Katholik', 'SD', 'Petani', 'Kawin', 'Tetap', 36, '2018-11-16 02:11:36', '0000-00-00 00:00:00', 5, ''),
(141, '7109015411930001', 'FARADILLA IRIANTI NARASIANG', 'KANANG', '1993-11-14', 'P', 'Hekang', 'Hekang', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '003', 'Katholik', 'SMP', 'IRT', 'Kawin', 'Tetap', 36, '2018-11-16 02:13:18', '0000-00-00 00:00:00', 5, ''),
(142, '7109010507120001', 'JULIO ALFA TAMPILANG', 'TATAHADENG', '2013-07-05', 'L', 'Hekang', 'Hekang', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '003', 'Katholik', 'SD', 'Siswa', 'Tidak Kawin', 'Tetap', 36, '2018-11-16 02:14:43', '0000-00-00 00:00:00', 5, ''),
(143, '7109011505720001', 'DOLFIUS TATUIL', 'PANIKI', '1972-05-15', 'L', 'Hekang', 'Hekang', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '003', 'Kristen', 'SMP', 'Wiraswasta', 'Kawin', 'Tetap', 36, '2019-11-16 02:26:06', '0000-00-00 00:00:00', 5, ''),
(144, '7109016201840003', 'JEFNY HARIATI TAMAKA', 'TATAHADENG', '1984-01-22', 'P', 'Hekang', 'Hekang', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '003', 'Kristen', 'SMA', 'IRT', 'Kawin', 'Tetap', 36, '2019-11-16 02:32:58', '0000-00-00 00:00:00', 5, ''),
(145, '7109012004770001', 'RIS PAULUS PONTOH', 'TATAHADENG', '1977-04-20', 'L', 'Hekang', 'Hekang', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '003', 'Kristen', 'SD', 'Petani', 'Kawin', 'Tetap', 37, '2019-11-16 02:46:25', '0000-00-00 00:00:00', 5, ''),
(146, '7109015604820001', 'YOKEBET TAMPILANG', 'TATAHADENG', '1982-04-16', 'P', 'Hekang', 'Hekang', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '003', 'Kristen', 'SD', 'IRT', 'Kawin', 'Tetap', 37, '2019-11-16 02:48:09', '0000-00-00 00:00:00', 5, ''),
(147, '7109010708080001', 'GOLDI RIYOK PONTOH', 'TATAHADENG', '2008-08-07', 'L', 'Hekang', 'Hekang', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '003', 'Kristen', 'SMP', 'Siswa', 'Tidak Kawin', 'Tetap', 37, '2019-11-16 02:49:38', '0000-00-00 00:00:00', 5, ''),
(148, '7109011311500001', 'STER TAMPILANG', 'TATAHADENG', '1950-11-13', 'L', 'Hekang', 'Hekang', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '003', 'Katholik', 'SD', 'Petani', 'Kawin', 'Tetap', 37, '2019-11-16 02:53:44', '0000-00-00 00:00:00', 5, ''),
(149, '7109016909520001', 'JASMAN KAKUNSI', 'TATAHADENG', '1952-09-29', 'P', 'Hekang', 'Hekang', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '003', 'Katholik', 'SD', 'IRT', 'Kawin', 'Tetap', 37, '2019-11-16 02:55:25', '0000-00-00 00:00:00', 5, ''),
(150, '7109010104660002', 'PETRUS KAWANGUNG', 'DAME', '1966-01-04', 'L', 'Hekang', 'Hekang', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '003', 'Kristen', 'SD', 'Petani', 'Kawin', 'Tetap', 37, '2019-11-16 03:09:35', '0000-00-00 00:00:00', 5, ''),
(151, '7109016907780001', 'ROS LIN TAMPILANG', 'TATAHADENG', '1978-07-29', 'P', 'Hekang', 'Hekang', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '003', 'Kristen', 'SD', 'IRT', 'Kawin', 'Tetap', 37, '2019-11-16 03:11:25', '0000-00-00 00:00:00', 5, ''),
(152, '7109010911620003', 'RONI PONTOH', 'TATAHADENG', '1962-11-09', 'L', 'Hekang', 'Hekang', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '003', 'Kristen', 'SD', 'Petani', 'Kawin', 'Tetap', 37, '2019-11-16 03:15:46', '0000-00-00 00:00:00', 5, ''),
(153, '7109016203550001', 'MARCE SALELE', 'TATAHADENG', '1955-03-22', 'P', 'Hekang', 'Hekang', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '003', 'Kristen', 'SD', 'IRT', 'Kawin', 'Tetap', 37, '2019-11-16 03:17:52', '0000-00-00 00:00:00', 5, ''),
(154, '7109031210590001', 'EDELSTON MARTHIN', 'ULU SIAU', '1959-10-12', 'L', 'leuwaha', 'leuwaha', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '003', 'Katholik', 'S1', 'PNS', 'Kawin', 'Tetap', 38, '2018-11-20 02:05:11', '0000-00-00 00:00:00', 6, ''),
(155, '7109035212630001', 'DIETS SYUL FRINE SAHEDE', 'DAME', '1963-12-12', 'P', 'leuwaha', 'leuwaha', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '003', 'Katholik', 'S1', 'PNS', 'Kawin', 'Tetap', 38, '2018-11-20 02:07:09', '0000-00-00 00:00:00', 6, ''),
(156, '7109035001050002', 'YIYIN MARTHIN', 'TAGULANDANG', '2005-01-10', 'P', 'leuwaha', 'leuwaha', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '003', 'Katholik', 'SD', 'Siswa', 'Tidak Kawin', 'Tetap', 38, '2018-11-20 02:08:57', '0000-00-00 00:00:00', 6, ''),
(157, '7109016503890001', 'YOFIAN MITUSALA', 'TATAHADENG', '1992-08-17', 'L', 'leuwaha', 'leuwaha', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '003', 'Kristen', 'SMA', 'Petani', 'Kawin', 'Tetap', 39, '2019-11-20 02:14:26', '0000-00-00 00:00:00', 6, ''),
(158, '7109011708920001', 'SATLY SUNDANA', 'DAME', '1989-03-25', 'P', 'Leuwaha', 'Leuwaha', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '003', 'Kristen', 'SMA', 'IRT', 'Kawin', 'Tetap', 39, '2019-11-20 02:16:04', '0000-00-00 00:00:00', 6, ''),
(159, '7109012506150001', 'JUNIOR FIANLY ALFA MITUSALA', 'TAHUNA', '2015-06-25', 'L', 'Leuwaha', 'Leuwaha', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '002', '003', 'Kristen', 'SD', '', 'Tidak Kawin', 'Tetap', 39, '2019-11-20 02:18:15', '0000-00-00 00:00:00', 6, ''),
(160, '7109015912360002', 'DINTJE MUMU', 'ULU SIAU', '2019-11-20', 'P', 'Lewae', 'Lewae', 'Tatahadeng', 'Siau Timur', 'Siau Tagulandang Biaro', 'Sulawesi Utara', 'Indonesia', '001', '001', 'Kristen', 'Tidak Sekolah', '', 'Tidak Kawin', 'Tetap', 23, '2018-11-20 11:05:33', '0000-00-00 00:00:00', 1, '1');

-- --------------------------------------------------------

--
-- Table structure for table `warga_has_kartu_keluarga`
--

CREATE TABLE `warga_has_kartu_keluarga` (
  `id_warga` int(11) NOT NULL,
  `id_keluarga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `warga_has_kartu_keluarga`
--

INSERT INTO `warga_has_kartu_keluarga` (`id_warga`, `id_keluarga`) VALUES
(6, 2),
(6, 40),
(7, 2),
(8, 2),
(9, 3),
(10, 3),
(11, 4),
(12, 4),
(13, 5),
(14, 5),
(15, 5),
(16, 5),
(17, 6),
(18, 6),
(19, 7),
(20, 7),
(21, 7),
(22, 8),
(23, 8),
(24, 9),
(25, 9),
(26, 9),
(27, 9),
(31, 11),
(32, 11),
(33, 11),
(34, 13),
(35, 13),
(36, 13),
(37, 13),
(38, 14),
(39, 14),
(40, 15),
(41, 15),
(42, 15),
(43, 15),
(44, 16),
(45, 16),
(46, 16),
(47, 17),
(48, 17),
(49, 17),
(50, 18),
(51, 18),
(52, 19),
(53, 19),
(54, 19),
(56, 19),
(57, 19),
(58, 25),
(59, 25),
(60, 25),
(62, 21),
(68, 21),
(70, 21),
(71, 21),
(72, 21),
(73, 21),
(74, 22),
(75, 22),
(76, 23),
(77, 23),
(78, 23),
(79, 23),
(80, 24),
(81, 24),
(82, 26),
(83, 26),
(84, 26),
(85, 26),
(86, 26),
(87, 26),
(88, 26),
(89, 27),
(90, 27),
(91, 27),
(92, 27),
(93, 28),
(94, 28),
(95, 28),
(96, 29),
(97, 29),
(98, 29),
(102, 31),
(103, 31),
(104, 31),
(105, 33),
(106, 32),
(107, 34),
(108, 34),
(109, 34),
(110, 34),
(113, 35),
(114, 35),
(115, 35),
(116, 35),
(117, 36),
(118, 36),
(119, 36),
(120, 37),
(121, 37),
(122, 37),
(123, 37),
(128, 38),
(129, 38),
(130, 38),
(131, 39),
(132, 39),
(133, 39),
(136, 41),
(137, 41),
(138, 41),
(139, 41),
(140, 42),
(141, 42),
(142, 42),
(143, 43),
(144, 43),
(145, 44),
(146, 44),
(147, 44),
(148, 45),
(149, 45),
(150, 46),
(151, 46),
(152, 47),
(153, 47),
(154, 50),
(155, 50),
(156, 50),
(157, 49),
(158, 49),
(159, 49);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kartu_keluarga`
--
ALTER TABLE `kartu_keluarga`
  ADD PRIMARY KEY (`id_keluarga`),
  ADD KEY `id_kepala_keluarga` (`id_kepala_keluarga`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `mutasi`
--
ALTER TABLE `mutasi`
  ADD PRIMARY KEY (`id_mutasi`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `warga`
--
ALTER TABLE `warga`
  ADD PRIMARY KEY (`id_warga`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `warga_has_kartu_keluarga`
--
ALTER TABLE `warga_has_kartu_keluarga`
  ADD KEY `id_penduduk` (`id_warga`,`id_keluarga`),
  ADD KEY `warga_has_kartu_keluarga_ibfk_2` (`id_keluarga`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kartu_keluarga`
--
ALTER TABLE `kartu_keluarga`
  MODIFY `id_keluarga` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `mutasi`
--
ALTER TABLE `mutasi`
  MODIFY `id_mutasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `warga`
--
ALTER TABLE `warga`
  MODIFY `id_warga` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=161;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `kartu_keluarga`
--
ALTER TABLE `kartu_keluarga`
  ADD CONSTRAINT `kartu_keluarga_ibfk_1` FOREIGN KEY (`id_kepala_keluarga`) REFERENCES `warga` (`id_warga`),
  ADD CONSTRAINT `kartu_keluarga_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);

--
-- Constraints for table `mutasi`
--
ALTER TABLE `mutasi`
  ADD CONSTRAINT `mutasi_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);

--
-- Constraints for table `warga_has_kartu_keluarga`
--
ALTER TABLE `warga_has_kartu_keluarga`
  ADD CONSTRAINT `warga_has_kartu_keluarga_ibfk_1` FOREIGN KEY (`id_warga`) REFERENCES `warga` (`id_warga`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `warga_has_kartu_keluarga_ibfk_2` FOREIGN KEY (`id_keluarga`) REFERENCES `kartu_keluarga` (`id_keluarga`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
